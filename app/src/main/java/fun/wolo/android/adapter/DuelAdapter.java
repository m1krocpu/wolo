package fun.wolo.android.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import fun.wolo.android.R;
import fun.wolo.android.model.m_duel;

public class DuelAdapter extends RecyclerView.Adapter<DuelAdapter.ViewHolder>{
    private ArrayList<m_duel> data = new ArrayList<>();
    private m_duel item;
    private Context context;
    private OnItemClickListener listener;
    private Calendar c = Calendar.getInstance();

    public DuelAdapter(Context context, ArrayList<m_duel> data, OnItemClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.duel_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.onBind(position, listener);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClickListener {
        void onItemClick(m_duel item, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        protected LinearLayout btDetail;
        protected TextView lblNomor, lblNick, lblLokasi;

        public ViewHolder(View itemView) {
            super(itemView);
            btDetail = itemView.findViewById(R.id.btDetail);

            lblNomor = itemView.findViewById(R.id.lblNoDuel);
            lblNick = itemView.findViewById(R.id.lblNick);
            lblLokasi = itemView.findViewById(R.id.lblLokasi);
        }

        public void onBind(final int position, final OnItemClickListener listener) throws ParseException{
            item = data.get(position);

            lblNomor.setText("Duel " + String.valueOf(item.getNomor()));
            lblNick.setText("@" + item.getNick());
            lblLokasi.setText(item.getLokasi());


            btDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item = data.get(position);
                    listener.onItemClick(item, position);
                }
            });
        }

    }
}
