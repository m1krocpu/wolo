package fun.wolo.android.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.R;

public class StatistikAdapter extends BaseAdapter{
    private ArrayList<HashMap<String,Object>> data = new ArrayList<>();
    private HashMap<String,Object> item;
    private Context context;


    public StatistikAdapter(Context context, ArrayList<HashMap<String,Object>> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.statistik_item, parent, false);

        TextView lblNama = v.findViewById(R.id.lblNama);
        TextView lblStatus = v.findViewById(R.id.lblStatus);
        TextView lblPoin = v.findViewById(R.id.lblPoin);

        TextView lblRank = v.findViewById(R.id.lblRank);
        ImageView imgRank = v.findViewById(R.id.imgRank);

        item = data.get(position);

        lblNama.setText(item.get("nama").toString());
        lblStatus.setText("W: "+AppConfig.NullString(item.get("menang")) + "\t\t\tD: "+AppConfig.NullString(item.get("seri")) + "\t\t\tL: "+AppConfig.NullString(item.get("kalah")));
        lblPoin.setText(AppConfig.number(AppConfig.NullNumber(item.get("poin"))));

        if (position==0) {
            imgRank.setImageResource(R.drawable.ic_first);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position==1) {
            imgRank.setImageResource(R.drawable.ic_second);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position==2) {
            imgRank.setImageResource(R.drawable.ic_third);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position>2) {
            lblRank.setText(String.valueOf(position+1));
            lblRank.setVisibility(View.VISIBLE);
            imgRank.setVisibility(View.GONE);
        }


        return v;
    }

}
