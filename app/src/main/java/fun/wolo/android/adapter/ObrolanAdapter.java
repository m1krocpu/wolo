package fun.wolo.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.R;
import fun.wolo.android.model.m_auth;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class ObrolanAdapter extends BaseAdapter{
    Context context;
    ArrayList<HashMap<String, Object>> data;
    HashMap<String, Object> item;
    m_auth auth;

    public ObrolanAdapter(Context context, ArrayList<HashMap<String, Object>> data){
        this.context = context;
        this.data = data;
        auth = new m_auth(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }




    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        view = LayoutInflater.from(context).inflate(R.layout.obrolan_item,parent,false);

        item = data.get(position);
        LinearLayout Anda = view.findViewById(R.id.anda);
        LinearLayout Player = view.findViewById(R.id.player);
        TextView lblTimeA =  view.findViewById(R.id.lblTimeA);
        EmojiconTextView lblAnda =  view.findViewById(R.id.lblAnda);
        TextView lblTimeP =  view.findViewById(R.id.lblTimeP);
        EmojiconTextView lblPlayer =  view.findViewById(R.id.lblPlayer);
        TextView imgPlayer =  view.findViewById(R.id.imgPlayer);
        TextView lblNamaPlayer =  view.findViewById(R.id.lblNamaPlayer);
        TextView lblGM =  view.findViewById(R.id.lblGM);

        String time = AppConfig.NullString(item.get("time"));
        if (AppConfig.formatDate(time).equalsIgnoreCase(AppConfig.format(AppConfig.getDate()))){
            time = AppConfig.formatTime(time);
        }

        if (item.get("usr").toString().equalsIgnoreCase(auth.getAkun().akun_nama)){
            //ANDA
            Anda.setVisibility(View.VISIBLE);
            Player.setVisibility(View.GONE);
            lblAnda.setText(AppConfig.NullString(item.get("msg")));
            lblTimeA.setText(time);
        }else{
            //PLAYER
            Player.setVisibility(View.VISIBLE);
            Anda.setVisibility(View.GONE);
            lblGM.setVisibility(View.GONE);
            lblPlayer.setText(AppConfig.NullString(item.get("msg")));
            lblTimeP.setText(time);
            lblNamaPlayer.setText(AppConfig.NullString(item.get("usr")));
            imgPlayer.setText(AppConfig.NullString(item.get("usr")).substring(0,1).toUpperCase());
            if (item.get("scope") != null){
                String scope = AppConfig.NullString(item.get("scope"));
                if (!scope.equalsIgnoreCase("0")){
                    lblGM.setVisibility(View.VISIBLE);
                    imgPlayer.setText("");
                    imgPlayer.setBackgroundResource(R.drawable.gm);
                }
            }
        }

        return view;
    }
}
