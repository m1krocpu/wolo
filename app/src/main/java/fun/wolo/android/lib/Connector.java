package fun.wolo.android.lib;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import fun.wolo.android.AppConfig;
import fun.wolo.android.model.m_akun;
import fun.wolo.android.model.m_auth;
import okhttp3.OkHttpClient;

public class Connector {
    private Context context;
    private OnCompletedListener listener;
    private String _token="";
    private m_auth auth;

    public Connector(Context context){
        this.context = context;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();

        AndroidNetworking.initialize(context,okHttpClient);
        auth = new m_auth(context);
    }

    public Connector cancel(){
        AndroidNetworking.forceCancelAll();
        return  this;
    }


    public Connector(Context context, String UID){
        this.context = context;
        AndroidNetworking.initialize(context);
        _token = AndroidNetworking.get(AppConfig.SERVER_API)
                .addHeaders("access_key",UID)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getHeaders().toString();
    }

    public interface OnCompletedListener{
        void OnCompleted(Object data);
        void OnFailed(int status);
    }


    public void check(final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API + "version")
                .addHeaders("User-Agent", "wolo-app")
                .addHeaders("X-Environment", "Android")
                .addHeaders("Connection", "close")
                .addHeaders("Accept", "application/json")
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject item) {
                        try {
                            listener.OnCompleted(item.get("version").toString());
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }

    public void ntp(final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API + "ntp")
                .addHeaders("User-Agent", "wolo-app")
                .addHeaders("X-Environment", "Android")
                .addHeaders("Connection", "close")
                .addHeaders("Accept", "application/json")
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject item) {
                        try {
                            long time = Long.parseLong(item.get("ntp").toString()) * 1000;
                            listener.OnCompleted(time);
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }

    public void login(String Username, String Password, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"oauth/authorize")
                .addBodyParameter("device", AppConfig.getDeviceName())
                .addBodyParameter("username", Username)
                .addBodyParameter("password",Password)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onResponse(final JSONObject item) {
                        new AsyncTask<Integer, Integer, Integer>() {

                            @Override
                            protected Integer doInBackground(Integer... params) {
                                try {
                                    if(item.get("exception").toString().equalsIgnoreCase("invalid_credentials")) {
                                        return 2;
                                    } else if(item.get("exception").toString().equalsIgnoreCase("")){
                                        if(item.get("status").toString().equalsIgnoreCase("0")){
                                            return 3;
                                        }else{
                                            m_akun akun = new m_akun();
                                            akun.akun_tanggal = item.get("tanggal").toString();
                                            akun.akun_token = item.get("access_token").toString();

                                            JSONObject data = item.getJSONObject("data");
                                            akun.akun_clientid = data.get("client_id").toString();
                                            akun.akun_clientsecret = data.get("client_secret").toString();
                                            akun.akun_nama = data.get("nama").toString();
                                            akun.akun_nik = Long.parseLong(data.get("nik").toString());
                                            akun.akun_desa = Long.parseLong(data.get("desa").toString());
                                            akun.akun_desa_nama = data.get("desa_nama").toString();
                                            akun.akun_kecamatan_nama = data.get("kecamatan_nama").toString();
                                            akun.akun_kota_nama = data.get("kota_nama").toString();

                                            auth.setPrefAkun(akun);
                                            auth.setToken(item.get("refresh_token").toString());
                                            return 1;
                                        }
                                    }
                                } catch (JSONException e) {
                                    Log.e("Connector JSON",e.getMessage());
                                }
                                return 0;
                            }

                            @Override
                            protected void onPostExecute(Integer result) {
                                super.onPostExecute(result);

                                if (result==1){
                                    listener.OnCompleted(null);
                                }else if (result==2){
                                    listener.OnFailed(2);
                                }else if (result==3){
                                    listener.OnFailed(3);
                                }else{
                                    Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                                    listener.OnFailed(0);
                                }
                            }
                        }.execute();

                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }
    public void reg_nik(String NIK, String NOMOR, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"oauth/nik")
                .addBodyParameter("device", AppConfig.getDeviceName())
                .addBodyParameter("nik", NIK)
                .addBodyParameter("hp",NOMOR)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onResponse(final JSONObject item) {
                        new AsyncTask<Integer, Integer, Integer>() {
                            @Override
                            protected Integer doInBackground(Integer... params) {
                                try {
                                    if(item.get("exception").toString().equalsIgnoreCase("already_registered")) {
                                        return 2;
                                    }else if(item.get("exception").toString().equalsIgnoreCase("not_found")) {
                                        return 3;
                                    }else if(item.get("success").toString().equalsIgnoreCase("1")){
                                        return 1;
                                    }
                                } catch (JSONException e) {
                                    Log.e("Connector JSON",e.getMessage());
                                }
                                return 0;
                            }

                            @Override
                            protected void onPostExecute(Integer result) {
                                super.onPostExecute(result);

                                if (result==1){
                                    listener.OnCompleted(null);
                                }else if (result==2){
                                    listener.OnFailed(2);
                                }else if (result==3){
                                    listener.OnFailed(3);
                                }else{
                                    Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                                    listener.OnFailed(0);
                                }
                            }
                        }.execute();
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }
    public void reg_manual(String NIK, String NOMOR, String NAMA, String DESA, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"oauth/registernik")
                .addBodyParameter("device", AppConfig.getDeviceName())
                .addBodyParameter("nik", NIK)
                .addBodyParameter("hp",NOMOR)
                .addBodyParameter("nama",NAMA)
                .addBodyParameter("desa",DESA)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onResponse(final JSONObject item) {
                        new AsyncTask<Integer, Integer, Integer>() {
                            @Override
                            protected Integer doInBackground(Integer... params) {
                                try {
                                    if(item.get("exception").toString().equalsIgnoreCase("already_registered")) {
                                        return 2;
                                    }else if(item.get("exception").toString().equalsIgnoreCase("desa_notfound")) {
                                        return 3;
                                    }else if(item.get("success").toString().equalsIgnoreCase("1")){
                                        return 1;
                                    }
                                } catch (JSONException e) {
                                    Log.e("Connector JSON",e.getMessage());
                                }
                                return 0;
                            }

                            @Override
                            protected void onPostExecute(Integer result) {
                                super.onPostExecute(result);

                                if (result==1){
                                    listener.OnCompleted(null);
                                }else if (result==2){
                                    listener.OnFailed(2);
                                }else if (result==3){
                                    listener.OnFailed(3);
                                }else{
                                    Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                                    listener.OnFailed(0);
                                }
                            }
                        }.execute();
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }
    public void reg_verifikasi(String NIK, String USERNAME, String PASSWORD, String KODE, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"oauth/register")
                .addBodyParameter("device", AppConfig.getDeviceName())
                .addBodyParameter("nik", NIK)
                .addBodyParameter("username", USERNAME)
                .addBodyParameter("password", PASSWORD)
                .addBodyParameter("kode", KODE)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("StaticFieldLeak")
                    @Override
                    public void onResponse(final JSONObject item) {
                        new AsyncTask<Integer, Integer, Integer>() {

                            @Override
                            protected Integer doInBackground(Integer... params) {
                                try {
                                    if(item.get("exception").toString().equalsIgnoreCase("invalid_code")) {
                                        return 2;
                                    }else if(item.get("exception").toString().equalsIgnoreCase("username_exists")) {
                                        return 3;
                                    }else if(item.get("exception").toString().equalsIgnoreCase("")){
                                        m_akun akun = new m_akun();
                                        akun.akun_tanggal = item.get("tanggal").toString();
                                        akun.akun_token = item.get("access_token").toString();

                                        JSONObject data = item.getJSONObject("data");
                                        akun.akun_clientid = data.get("client_id").toString();
                                        akun.akun_clientsecret = data.get("client_secret").toString();
                                        akun.akun_nama = data.get("nama").toString();
                                        akun.akun_nik = Long.parseLong(data.get("nik").toString());
                                        akun.akun_desa = Long.parseLong(data.get("desa").toString());
                                        akun.akun_desa_nama = data.get("desa_nama").toString();
                                        akun.akun_kecamatan_nama = data.get("kecamatan_nama").toString();
                                        akun.akun_kota_nama = data.get("kota_nama").toString();

                                        auth.setPrefAkun(akun);
                                        auth.setToken(item.get("refresh_token").toString());
                                        return 1;
                                    }
                                } catch (JSONException e) {
                                    Log.e("Connector JSON",e.getMessage());
                                }
                                return 0;
                            }

                            @Override
                            protected void onPostExecute(Integer result) {
                                super.onPostExecute(result);

                                if (result==1){
                                    listener.OnCompleted(null);
                                }else if (result==2){
                                    listener.OnFailed(2);
                                }else if (result==3){
                                    listener.OnFailed(3);
                                }else{
                                    Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                                    listener.OnFailed(0);
                                }
                            }
                        }.execute();
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }

    public void token(final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"oauth/token")
                .addBodyParameter("device", AppConfig.getDeviceName())
                .addBodyParameter("grant_type", "refresh_token")
                .addBodyParameter("client_id",auth.getAkun().akun_clientid)
                .addBodyParameter("client_secret", auth.getAkun().akun_clientsecret)
                .addBodyParameter("code", auth.getToken())
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject item) {
                        try {
                            if (item.get("token_type")==null){
                                listener.OnFailed(0);
                            }else if (item.get("status").toString().equalsIgnoreCase("0")){
                                listener.OnFailed(1);
                            }else if(item.get("token_type").toString().equalsIgnoreCase("Bearer")){

                                auth.setTanggal(item.get("tanggal").toString());
                                auth.setToken(item.get("refresh_token").toString());
                                auth.setAccess(item.get("access_token").toString());

                                listener.OnCompleted(null);
                            }else{
                                listener.OnFailed(0);
                            }
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }


    public void getDesa(final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"desa")
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listener.OnCompleted(response.toString());
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());

                        listener.OnFailed(0);
                    }
                });
    }

    public void getKuis(String ID, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"kuis")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("id", ID)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            String KUIS = response.getString("kuis");
                            listener.OnCompleted(KUIS);
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(1);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());

                        listener.OnFailed(0);
                    }
                });
    }

    public void setKuis(String ID, String JAWAB){
        AndroidNetworking.post(AppConfig.SERVER_API+"setKuis")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("id", ID)
                .addBodyParameter("data", JAWAB)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e("Connector",response.toString());
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                    }
                });
    }

    public void setObrolan(String STATE, String CHAT){
        AndroidNetworking.post(AppConfig.SERVER_API+"obrolan")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("state", STATE)
                .addBodyParameter("data", CHAT)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e("Connector",response.toString());
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                    }
                });
    }

    public void getDuel(String ID, String KATEGORI, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"duel")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("id", ID)
                .addBodyParameter("k", KATEGORI)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            String SOAL = response.getString("soal");
                            if (!SOAL.trim().equalsIgnoreCase("")){
                                listener.OnCompleted(response.toString());
                            }else{
                                listener.OnFailed(0);
                            }
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }

    public void setDuel(final OnCompletedListener listener){
        AndroidNetworking.post(AppConfig.SERVER_API+"setDuel")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()

                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("success").equalsIgnoreCase("1")){
                                listener.OnCompleted(response.get("room").toString());
                            }else{
                                listener.OnFailed(1);
                            }
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(1);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }


    public void joinDuel(String ROOM, final OnCompletedListener listener){
        AndroidNetworking.post(AppConfig.SERVER_API+"joinDuel")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("room", ROOM)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("success").equalsIgnoreCase("1")){
                                listener.OnCompleted(null);
                            }else{
                                listener.OnFailed(1);
                            }
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(1);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        Toast.makeText(context, "Koneksi ke server gagal\nSilahkan coba lagi atau cek koneksi internet Anda.", Toast.LENGTH_SHORT).show();
                        listener.OnFailed(0);
                    }
                });
    }

    public void soalDuel(String ROOM){
        AndroidNetworking.post(AppConfig.SERVER_API+"soalDuel")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("room", ROOM)
                .setTag(context)
                .setPriority(Priority.HIGH)
                .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.e("Connector",response.toString());
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                    }
                });
    }

    public void jawabDuel(String ID, String SOAL, String JAWAB, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"submit")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("id", ID)
                .addBodyParameter("soal", SOAL)
                .addBodyParameter("jawab", JAWAB)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if (response.getString("success").equalsIgnoreCase("1")){
                                listener.OnCompleted(null);
                            }else{
                                listener.OnFailed(0);
                            }
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }

    public void getKuisWinner(String ID, final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"kuisWinner")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .addBodyParameter("id", ID)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            String WINNER = response.getString("winner");
                            listener.OnCompleted(WINNER);
                        } catch (JSONException e) {
                            Log.e("Connector JSON",e.getMessage());
                            listener.OnFailed(0);
                        }
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }
    public void statistik(final OnCompletedListener listener){
        this.listener = listener;
        AndroidNetworking.post(AppConfig.SERVER_API+"statistik")
                .addBodyParameter("access_token", auth.getAkun().akun_token)
                .setTag(context)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.OnCompleted(response.toString());
                    }

                    @Override
                    public void onError(ANError e) {
                        Log.e("Connector",e.getMessage());
                        listener.OnFailed(0);
                    }
                });
    }
}
