package fun.wolo.android.lib;

import android.net.Uri;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyBrowser  extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Uri url = Uri.parse("android.resource://fun.wolo.android/raw/notfound");
        view.loadUrl("file:///android_res/raw/notfound.html");

    }


}
