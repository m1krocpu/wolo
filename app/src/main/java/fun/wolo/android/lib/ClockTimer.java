package fun.wolo.android.lib;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import fun.wolo.android.AppConfig;
import fun.wolo.android.model.m_auth;

public class ClockTimer {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    Date current;
    Calendar calendar = Calendar.getInstance();

    CountDownTimer timer;
    long millis = 24 * 60 * 1000;
    long interval = 1000;
    m_auth auth;
    Connector conn;

    public ClockTimer(Context context){
        conn = new Connector(context);
        auth = new m_auth(context);
    }

    private void run(){
        getDate();
        timer = new CountDownTimer(millis, interval) {
            @Override
            public void onTick(long millisUntilFinished) {
                calendar.setTimeInMillis(current.getTime() + interval);

                current = calendar.getTime();
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    public interface OnCompletedListener{
        void OnCompleted();
    }


    public ClockTimer start(final OnCompletedListener listener){
        current = calendar.getTime();
        conn.ntp(new Connector.OnCompletedListener() {
            @Override
            public void OnCompleted(Object data) {
                long now = Long.parseLong(data.toString());
                current = new Date(now);
                listener.OnCompleted();
                run();
            }

            @Override
            public void OnFailed(int status) {
                current = calendar.getTime();
                listener.OnCompleted();
                run();
            }
        });
        return this;
    }

    public ClockTimer restart(){
        if (timer!=null){
            timer.cancel();
        }
        start(new OnCompletedListener() {
            @Override
            public void OnCompleted() {

            }
        });
        return this;
    }


    public ClockTimer stop(){
        timer.cancel();
        return this;
    }
    public String getDateString(){
        if (current==null){
            return auth.getAkun().akun_tanggal;
        }
        return AppConfig.convertDateTime(df.format(current)).substring(0,10);
    }

    public Date getDate(){
        try {
            return df.parse(AppConfig.convertDateTime(df.format(current)));
        } catch (ParseException e) {
            Log.e("ERR PARSE CLOCK",e.getMessage());
            return current;
        } catch (NullPointerException e){
            Log.e("ERR NULL",e.getMessage());
            return current;
        }
    }


}
