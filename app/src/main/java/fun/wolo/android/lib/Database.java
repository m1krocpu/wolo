package fun.wolo.android.lib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fun.wolo.android.AppConfig;

public class Database {
    private dbHelper helper;
    private SQLiteDatabase db;

    public Database(Context context){
        helper = new dbHelper(context);
    }

    public Database Open(){
        db = helper.getWritableDatabase();
        return this;
    }

    public void clearKuis(){
        db.execSQL("DELETE FROM tb_kuis");
    }


    public boolean set(String table, HashMap<String,Object> data, String selection, String[] selectionArgs){
        ContentValues values = new ContentValues();
        for(Map.Entry<String, Object> entry : data.entrySet()) {
            values.put(entry.getKey(),entry.getValue().toString());
        }
        Cursor customer = db.query(table,null,selection, selectionArgs,null,null,null);
        if (customer.getCount()>0){
            long proses = db.update(table,values,selection, selectionArgs);
            if (proses > 0) {
                return true;
            }
        }else {
            long proses = db.insert(table, null, values);
            if (proses > 0) {
                return true;
            }
        }
        return false;
    }

    public boolean insert(String table, HashMap<String,Object> data){
        ContentValues values = new ContentValues();
        for(Map.Entry<String, Object> entry : data.entrySet()) {
            values.put(entry.getKey(),entry.getValue().toString());
        }

        long proses = db.insert(table,null,values);
        if (proses>0){
            return true;
        }
        return false;
    }

    public ArrayList<HashMap<String,Object>> getKuis(String tanggal){
        ArrayList<HashMap<String,Object>> data = new ArrayList<>();
        HashMap<String,Object> item;
        Cursor cursor = db.query("tb_kuis", null, "tanggal=?",new String[]{tanggal}, null, null, "mulai ASC");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                item = new HashMap<>();
                for (String column: cursor.getColumnNames()){
                    item.put(column, AppConfig.NullString(cursor.getString(cursor.getColumnIndex(column))));
                }
                data.add(item);

                cursor.moveToNext();
            }
        }
        return data;
    }
}
