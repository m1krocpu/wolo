package fun.wolo.android.lib;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbHelper extends SQLiteOpenHelper {
    private static String name = "wolo.db";
    private static int version = 1;
    private static String tb_kuis = "CREATE TABLE tb_kuis(id INTEGER PRIMARY KEY, tanggal VARCHAR(12), mulai VARCHAR(12), selesai VARCHAR(12), hadiah INTEGER)";

    public dbHelper(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tb_kuis);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
