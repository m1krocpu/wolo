package fun.wolo.android.lib;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fun.wolo.android.R;


public class PromptLoading extends AlertDialog {
    TextView btnBATAL, btnOK, message;
    LinearLayout layoutAction;
    public PromptLoading(@NonNull Context context) {
        super(context);
        setCancelable(false);
        View v = getLayoutInflater().inflate(R.layout.dialog_loading,null);

        message = v.findViewById(R.id.message);
        layoutAction = v.findViewById(R.id.layoutAction);
        btnOK = v.findViewById(R.id.btnOK);
        btnBATAL = v.findViewById(R.id.btnBATAL);
        btnBATAL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutAction.setVisibility(View.VISIBLE);
            }
        }, 12000);
        setView(v);
    }

    @Override
    public void setMessage(CharSequence msg) {
        message.setText(msg);
        message.setVisibility(View.VISIBLE);
    }

    public PromptLoading setOnTrueListener(View.OnClickListener listener){
        btnOK.setVisibility(View.VISIBLE);
        btnOK.setOnClickListener(listener);
        return this;
    }
    public PromptLoading setOnFalseListener(View.OnClickListener listener){
        btnBATAL.setOnClickListener(listener);
        return this;
    }

}
