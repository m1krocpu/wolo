package fun.wolo.android.lib;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import fun.wolo.android.R;

public class PromptInfo extends AlertDialog {

    public PromptInfo(@NonNull Context context, String msg) {
        super(context);
        View v = getLayoutInflater().inflate(R.layout.dialog_info, null);
        setView(v);

        TextView lblInfo = v.findViewById(R.id.lblInfo);
        lblInfo.setText(msg);
        Button btnOK = v.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }
}
