package fun.wolo.android.model;

public class m_duel {
    private long Nomor;
    private String Nick, Lokasi;

    public long getNomor() {
        return Nomor;
    }

    public void setNomor(long nomor) {
        Nomor = nomor;
    }

    public String getNick() {
        return Nick;
    }

    public void setNick(String nick) {
        Nick = nick;
    }

    public String getLokasi() {
        return Lokasi;
    }

    public void setLokasi(String lokasi) {
        Lokasi = lokasi;
    }
}
