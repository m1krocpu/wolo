package fun.wolo.android.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

public class m_auth {
    private Context context;
    m_akun data;

    private boolean LOGGED;
    private SharedPreferences prefs;

    public m_auth(Context context){
        this.context = context;
        prefs = context.getSharedPreferences("WOLO_FUN", AppCompatActivity.MODE_PRIVATE);
    }

    public boolean isLogged(){
        LOGGED = prefs.getBoolean("logged", false);

        if (!LOGGED){
            return false;
        }
        setAkun();
        return true;
    }

    public SharedPreferences getPreferences()
    {
        return prefs;
    }

    private void setAkun(){
        data = new m_akun();
        data.akun_nik = prefs.getLong("nik",0);
        data.akun_desa = prefs.getLong("desa",0);
        data.akun_desa_nama = prefs.getString("desa_nama",null);
        data.akun_kecamatan_nama = prefs.getString("kecamatan_nama",null);
        data.akun_kota_nama = prefs.getString("kota_nama",null);
        data.akun_nama = prefs.getString("nama",null);
        data.akun_tanggal = prefs.getString("tanggal",null);
        data.akun_clientid = prefs.getString("client_id",null);
        data.akun_clientsecret = prefs.getString("client_secret",null);
        data.akun_token = prefs.getString("access_token",null);
    }

    public void setPrefAkun(m_akun item){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("nik", item.akun_nik);
        editor.putLong("desa", item.akun_desa);
        editor.putString("desa_nama", item.akun_desa_nama);
        editor.putString("kecamatan_nama", item.akun_kecamatan_nama);
        editor.putString("kota_nama", item.akun_kota_nama);
        editor.putString("nama", item.akun_nama);
        editor.putString("tanggal", item.akun_tanggal);
        editor.putString("client_id", item.akun_clientid);
        editor.putString("client_secret", item.akun_clientsecret);
        editor.putString("access_token", item.akun_token);
        editor.putString("room", "");
        editor.putBoolean("logged", true);
        editor.commit();

        setAkun();
    }

    public void setTanggal(String tanggal){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("tanggal", tanggal);
        editor.commit();
    }

    public void setToken(String token){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("refresh_token", token);
        editor.commit();
    }


    public void setLastRoom(String room){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("room", room);
        editor.commit();
    }
    public String getLastRoom(){
        return prefs.getString("room",null);
    }


    public void setAccess(String token){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("access_token", token);
        editor.commit();
    }

    public String getToken(){
        return prefs.getString("refresh_token",null);
    }


    public m_akun getAkun(){
        setAkun();
        return data;
    }


    public void logout(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("logged",false);
        editor.commit();
    }
}
