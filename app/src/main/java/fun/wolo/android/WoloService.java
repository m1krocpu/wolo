package fun.wolo.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import fun.wolo.android.lib.Database;
import fun.wolo.android.model.m_auth;
import fun.wolo.android.view.Maintenance;
import fun.wolo.android.view.harian.HarianHome;

public class WoloService extends Service {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date1, date2;
    m_auth auth;
    Database db;
    CountDownTimer timer;
    ArrayList<HashMap<String, Object>> data = new ArrayList<>();
    boolean notif = false;
    HashMap<String, Object> maintenance = new HashMap<>();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public class MyBinder extends Binder {
        public WoloService getService() {
            return WoloService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        auth = new m_auth(this);
        db = new Database(this);

        db.Open();
        getDataFirebase();
        setClock();
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Launcher.clock.restart();

        return START_STICKY;
    }

    private void getDataFirebase() {
        DatabaseReference firebaseDB = FirebaseDatabase.getInstance().getReference();
        firebaseDB.child("maintenance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrInfo = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    maintenance = new HashMap<>();
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot info = arrInfo.iterator().next();
                        maintenance.put(info.getKey(), info.getValue());
                    }
                    if (maintenance.size()>0){
                        if(maintenance.get("status").toString().equalsIgnoreCase("1") && maintenance.get("trial").toString().equalsIgnoreCase("0") ){
                            if (Launcher.isActivityVisible()) {
                                Intent intent = new Intent(getApplicationContext(), Maintenance.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("KET", AppConfig.NullString(maintenance.get("ket")).trim());
                                startActivity(intent);
                            }
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR INFO",databaseError.getMessage());
            }
        });
        String TGL = auth.getAkun().akun_tanggal;
        if (Launcher.clock.getDateString()!=null){
            TGL = Launcher.clock.getDateString();
        }
        firebaseDB.child("kuis").child("jadwal").child(TGL).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                data.clear();
                Iterable<DataSnapshot> arrKuis = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Kuis = arrKuis.iterator().next();
                        String KEY = Kuis.getKey();
                        HashMap<String, Object> dataKuis = new HashMap<>();
                        dataKuis.put("id",KEY);
                        dataKuis.put("tanggal",Launcher.clock.getDateString());
                        if (Kuis.getChildrenCount()>0) {
                            Iterable<DataSnapshot> column = Kuis.getChildren();
                            for (int j=0; j<Kuis.getChildrenCount();j++){
                                DataSnapshot data = column.iterator().next();
                                if (data.getKey().equalsIgnoreCase("mulai")){
                                    if (data.getValue().toString().length()==7){
                                        dataKuis.put(data.getKey(), "0" + data.getValue());
                                    }else{
                                        dataKuis.put(data.getKey(), data.getValue());
                                    }
                                }else  if (data.getKey().equalsIgnoreCase("selesai")){
                                    if (data.getValue().toString().length()==7){
                                        dataKuis.put(data.getKey(), "0" + data.getValue());
                                    }else{
                                        dataKuis.put(data.getKey(), data.getValue());
                                    }
                                }else{
                                    dataKuis.put(data.getKey(), data.getValue());
                                }
                            }
                            db.set("tb_kuis",dataKuis,"id=?", new String[]{KEY});
                        }
                        data = db.getKuis(Launcher.clock.getDateString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR KUIS",databaseError.getMessage());
            }
        });
    }

    private void setClock(){
        if (timer !=null){
            timer.cancel();
        }
        long millis = 24 * 60 * 1000;
        timer = new CountDownTimer(millis, 1000) {
            public void onTick(long millisUntilFinished) {
                if (auth.isLogged()){
                    int index=0;
                    HashMap<String, Object> lastData =  new HashMap<>();
                    for(HashMap<String, Object> dataKuis: data){
                        if (index>0){
                            lastData = data.get(index-1);
                            if (lastData!=null){
                                Launcher.LASTIDKUIS  = lastData.get("id").toString();
                            }
                        }
                        try {
                            date1 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("mulai").toString());
                            date2 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("selesai").toString());
                            long millis = date1.getTime() - Launcher.clock.getDate().getTime();
                            Launcher.NOWKUIS = dataKuis;

                            if (millis >=0){
                                Launcher.IDKUIS = dataKuis.get("id").toString();
                                if (millis <= (5 * 60 * 1000) && !notif){
                                    notif = true;
                                    showAlarm();
                                }
                                break;
                            }else{
                                if (date2.getTime() - Launcher.clock.getDate().getTime()>0){
                                    notif = false;
                                    Launcher.IDKUIS = dataKuis.get("id").toString();
                                    break;
                                    //showAlarmProgress();
                                }else {
                                    if (date2.getTime() - Launcher.clock.getDate().getTime() + (3*60*1000)>=0){
                                        //showAlarmFinish();

                                    }
                                    if (data.size()== (index+1)){
                                        Launcher.IDKUIS = "-";
                                        Launcher.KUISSELESAI = true;
                                        Launcher.LASTIDKUIS = dataKuis.get("id").toString();
                                    }else{
                                        Launcher.IDKUIS = dataKuis.get("id").toString();
                                    }
                                }
                            }
                        }catch (ParseException e) {
                            e.printStackTrace();
                        }
                        index ++;
                    }
                }
            }

            public void onFinish() {

            }
        }.start();
    }


    private void setAlarm(HashMap<String, Object> dataKuis){
        try {
            date1 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("mulai").toString());
            date2 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("selesai").toString());
            long millis = date1.getTime() - Launcher.clock.getDate().getTime();

            if (millis >= (5 * 60 * 1000)) {
                millis -= (5 * 60 * 1000);
                if (timer!=null){
                    timer.cancel();
                }
                timer = new CountDownTimer(millis, 1000) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        showAlarm();
                    }
                }.start();
            }else if (millis >=0 ){
                showAlarm();
            }else{
                if (date2.getTime() - Launcher.clock.getDate().getTime()>0){
                    showAlarmProgress();
                }else if (date2.getTime() - Launcher.clock.getDate().getTime() + (3*60*1000)>=0){
                    showAlarmFinish();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void showAlarm(){
        //if (!Launcher.isActivityVisible()) {
        Intent notificationIntent = new Intent(getApplicationContext(), HarianHome.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(HarianHome.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        Notification notification = builder.setContentTitle("Kuis Harian Segera Dimulai!")
                .setContentText("Ayo buruan buka WOLO FUN")
                .setTicker("Kuis Harian Segera Dimulai!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(new long[]{300, 300, 300, 300, 300})
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent).build();

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
        //}

    }

    private void showAlarmProgress(){
        if (!Launcher.isActivityVisible()) {
            Intent notificationIntent = new Intent(getApplicationContext(), HarianHome.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(HarianHome.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            Notification notification = builder.setContentTitle("Kuis Harian Telah Dimulai!")
                    .setContentText("Ayo buruan buka WOLO FUN")
                    .setTicker("Kuis Harian Telah Dimulai!")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setVibrate(new long[]{300, 300, 300, 300, 300})
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
    }


    private void showAlarmFinish(){
        if (!Launcher.isActivityVisible()) {
            Intent notificationIntent = new Intent(getApplicationContext(), HarianHome.class);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(HarianHome.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            Notification notification = builder.setContentTitle("Kuis Harian Telah Selesai!")
                    .setContentText("Selamat Kepada Pemenang Kuis Harian")
                    .setTicker("Kuis Harian Telah Selesai!")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setVibrate(new long[]{300, 300, 300, 300, 300})
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
    }
}
