package fun.wolo.android;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.google.firebase.FirebaseApp;

import java.util.HashMap;

import fun.wolo.android.lib.ClockTimer;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Launcher extends Application {
    private static Launcher mInstance;
    public static Intent intent;
    private static boolean activityVisible = true;
    private static WoloService m_service;

    public static ClockTimer clock;

    public static String IDKUIS="", LASTIDKUIS="";
    public static HashMap<String, Object> NOWKUIS =  new HashMap<>();
    public static boolean KUISSELESAI = false;
    public static HashMap<String,HashMap<String, Object>> DUELSOAL = new HashMap<>();

    @Override
    public void onCreate(){
        super.onCreate();
        FirebaseApp.initializeApp(this);
        mInstance = this;


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/aer.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }


    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    public static ServiceConnection m_serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            m_service = ((WoloService.MyBinder)service).getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            m_service = null;
        }
    };

    public static synchronized Launcher getInstance() {
        return mInstance;
    }
}
