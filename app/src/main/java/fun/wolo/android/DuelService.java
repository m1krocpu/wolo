package fun.wolo.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import fun.wolo.android.view.duel.DuelBuat;

public class DuelService extends Service {
    private String ROOM;
    ValueEventListener handler;
    DatabaseReference firebaseDB;
    public DuelService(String ROOM){
        this.ROOM = ROOM;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        firebaseDB = FirebaseDatabase.getInstance().getReference();
        handler = firebaseDB.child("duel").child("run").child(ROOM).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                        DataSnapshot Room = arrRoom.iterator().next();
                        if (Room.getKey().equalsIgnoreCase("p2")) {
                            showNotif();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR DUEL RUN", databaseError.getMessage());
            }
        });
    }

        private void showNotif(){
            firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(handler);

            Intent notificationIntent = new Intent(this, DuelBuat.class);
            notificationIntent.putExtra("ROOM",ROOM);
            notificationIntent.putExtra("STATE","CREATE");

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(DuelBuat.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            Notification notification = builder.setContentTitle("Duel Kuis telah menemukan Lawan")
                    .setContentText("Klik Notifikasi Ini untuk melanjutkan")
                    .setTicker("Duel Kuis telah menemukan Lawan")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setVibrate(new long[]{300, 300, 300, 300, 300})
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
}
