package fun.wolo.android;


import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import fun.wolo.android.view.Main;


public class AppConfig {
    public static NotificationManager notificationManager = (NotificationManager) Launcher.getInstance().getSystemService(Launcher.getInstance().NOTIFICATION_SERVICE);

    public static Main MainApp = new Main();
    //public static String SERVER_API = "http://192.168.100.4:8000/api/v1/";
    public static String SERVER_API = "http://103.28.52.16/game/api/v1/";

    public final static String bulan[] = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni"
            , "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
    public final static String bulanShort[] = new String[]{"Jan", "Feb", "Mar", "Apr", "Mei", "Jun"
            , "Jul", "Ags", "Spt", "Okt", "Nov", "Des"};

    public static String number(long harga){
        NumberFormat num = NumberFormat.getInstance(Locale.GERMAN);

        return num.format(harga);
    }

    public static String convertDateTime(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getDefault());
        Date value = null;
        try {
            value = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        String dt = dateFormatter.format(value);

        return dt;
    }
    public static String convertTime(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getDefault());
        Date value = null;
        try {
            value = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        String dt = dateFormatter.format(value);

        return dt;
    }

    public static String format(String tanggal){
        return tanggal.substring(8,10)+'/'+tanggal.substring(5,7)+'/'+tanggal.substring(0,4);
    }
    public static String formatLongDate(String tanggal){
        return tanggal.substring(8,10)+' '+bulan[Integer.parseInt(tanggal.substring(5,7))-1]+' '+tanggal.substring(0,4);
    }
    public static String formatLongDateTime(String tanggal){
        return tanggal.substring(8,10)+' '+bulan[Integer.parseInt(tanggal.substring(5,7))-1]+' '+tanggal.substring(0,4) + " " + tanggal.substring(11,tanggal.length());
    }

    public static String NullString(Object obj){
        if (obj == null){
            return "";
        }else if (obj.toString().equalsIgnoreCase("null")){
            return "";
        }
        return obj.toString();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public static Integer NullNumber(Object obj){
        if (obj == null || AppConfig.NullString(obj).equalsIgnoreCase("")){
            return 0;
        }
        return Integer.parseInt(obj.toString());
    }

    public static String getDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat curDate = new SimpleDateFormat("yyyy-MM-dd");
        return curDate.format(c.getTime());
    }

    public static String getDate(String DATE){
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            Date mDate = fmt.parse(DATE);
            SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
            return fm.format(mDate);
        } catch (ParseException e) {
            return DATE;
        }
    }


    public static String getDateTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat curDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return curDate.format(c.getTime());
    }
    public static String getTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat curDate = new SimpleDateFormat("HH:mm:ss");
        return curDate.format(c.getTime());
    }


    public static String formatDate(String DATETIME){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = fmt.parse(DATETIME);
            SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");
            return fm.format(mDate);
        } catch (ParseException e) {
            return DATETIME;
        }catch (NullPointerException e) {
            return "";
        }
    }

    public static String formatDatetime(String DATETIME){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = fmt.parse(DATETIME);
            SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            return fm.format(mDate);
        } catch (ParseException e) {
            return DATETIME;
        }
    }
    public static String formatTime(String DATETIME){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = fmt.parse(DATETIME);
            SimpleDateFormat fm = new SimpleDateFormat("HH:mm:ss");
            return fm.format(mDate);
        } catch (ParseException e) {
            return DATETIME;
        }
    }


    public static Bitmap decodeBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.NO_WRAP);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    public static String getStatus(String status){
        switch (status){
            case "99": return "REQ.SPV";
            case "90": return "REQ.ADH";
            case "9": return "INVALID";
            case "1": return "VALID";
            case "2": return "MATCHED";
            case "3": return "DO";
            case "4": return "GI";
            case "10": return "CANCEL";
            case "11": return "REQ.CANCEL";
        }
        return "";
    }

    public static void expand(final View v, final int targetHeight) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.MATCH_PARENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    public static int collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);

        return initialHeight;
    }

    public static boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }



    public static void showNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(Launcher.getInstance());
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(Launcher.getInstance().getResources(), R.mipmap.ic_launcher));
        builder.setContentTitle("Sales Force Automation");
        builder.setContentText("Connecting...");
        notificationManager.notify(0, builder.build());
    }
}
