package fun.wolo.android.view.authentication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegistrasiStatus extends AppCompatActivity {
    boolean PassState = false;
    ImageView btShowP;
    LinearLayout btRegistrasi;
    String NIK, USERNAME, PASSWORD, KODE;
    TextView lblNIK;
    FrameLayout overlay;
    ProgressDialog pDialog;
    Connector conn;

    EditText etUSR, etPWD, etKode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi_status);
        NIK = getIntent().getStringExtra("NIK");
        conn = new Connector(this);

        overlay = findViewById(R.id.overlay);

        lblNIK = findViewById(R.id.lblNIK);
        lblNIK.setText("NIK : " + NIK);

        etUSR = findViewById(R.id.etUSR);
        etPWD = findViewById(R.id.etPWD);
        etKode = findViewById(R.id.etKode);
        btShowP = findViewById(R.id.btShowP);
        btShowP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PassState){
                    PassState = true;
                    etPWD.setTransformationMethod(null);
                    btShowP.setImageResource(R.drawable.ic_password_unshow);
                }else{
                    PassState = false;
                    etPWD.setTransformationMethod(new PasswordTransformationMethod());
                    btShowP.setImageResource(R.drawable.ic_password_show);
                }
            }
        });

        btRegistrasi = findViewById(R.id.btRegistrasi);
        btRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etUSR.getText().toString().trim().equalsIgnoreCase("") && !etPWD.getText().toString().trim().equalsIgnoreCase("")&& !etKode.getText().toString().trim().equalsIgnoreCase("")){
                    View vKey = RegistrasiStatus.this.getCurrentFocus();
                    if (vKey != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                    }
                    USERNAME = etUSR.getText().toString().trim();
                    PASSWORD = etPWD.getText().toString().trim();
                    KODE = etKode.getText().toString().trim();

                    overlay.setVisibility(View.VISIBLE);

                    pDialog = new ProgressDialog(RegistrasiStatus.this);
                    pDialog.setMessage("Verifikasi Akun...");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);
                    pDialog.show();

                    conn.reg_verifikasi(NIK, USERNAME, PASSWORD, KODE, new Connector.OnCompletedListener() {
                        @Override
                        public void OnCompleted(Object data) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            Intent intent = new Intent(RegistrasiStatus.this, RegistrasiSukses.class);
                            intent.putExtra("USERNAME",USERNAME);
                            startActivity(intent);
                            setResult(1);
                            finish();
                        }

                        @Override
                        public void OnFailed(int status) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            if (status==2){
                                Toast.makeText(RegistrasiStatus.this, "Kode Verfikasi tidak sesuai!",Toast.LENGTH_LONG).show();
                            }else if (status==3){
                                Toast.makeText(RegistrasiStatus.this, "Username telah digunakan!",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(RegistrasiStatus.this, "Semua Rincian Akun belum diisi dengan benar!",Toast.LENGTH_LONG).show();
                    etUSR.requestFocus();
                }

            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onBackPressed() {
        setResult(0);
        super.onBackPressed();
    }
}
