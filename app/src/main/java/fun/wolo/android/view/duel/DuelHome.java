package fun.wolo.android.view.duel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.google.firebase.database.FirebaseDatabase;

import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptInfo;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DuelHome extends AppCompatActivity {
    Connector conn;
    m_auth auth;
    ProgressDialog pDialog;
    FrameLayout overlay;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_duel_home);

        conn = new Connector(this);
        auth = new m_auth(this);
        overlay = findViewById(R.id.overlay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!auth.getLastRoom().equalsIgnoreCase("")) {
            FirebaseDatabase.getInstance().getReference().child("duel").child("room").child(auth.getLastRoom()).removeValue();
            auth.setLastRoom("");
        }
        Launcher.activityResumed();
    }

    public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    public void showDuelBuat(View v){
        overlay.setVisibility(View.VISIBLE);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Membuat Duel Kuis...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        conn.setDuel(new Connector.OnCompletedListener() {
            @Override
            public void OnCompleted(Object data) {
                auth.setLastRoom(data.toString());

                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                Intent intent = new Intent(DuelHome.this, DuelBuat.class);
                intent.putExtra("STATE","CREATE");
                intent.putExtra("ROOM",data.toString());
                startActivity(intent);
            }

            @Override
            public void OnFailed(int status) {
                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                if (status==1){
                    new PromptInfo(DuelHome.this,"Duel Kuis gagal dibuat, silahkan coba lagi!").show();
                }
            }
        });
    }

    public void showDuelCari(View v){
        Intent intent = new Intent(this, DuelCari.class);
        startActivity(intent);
    }
}
