package fun.wolo.android.view.statistik;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.adapter.StatistikAdapter;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptLoading;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Statistik extends AppCompatActivity {
    String STATE = "kelurahan";
    m_auth auth;
    SwipeRefreshLayout layout;
    ListView lvData;
    StatistikAdapter adapter;
    HashMap<String,ArrayList<HashMap<String, Object>>> arrData = new HashMap<>();
    ArrayList<HashMap<String, Object>> data;
    HashMap<String, Object> item;
    HashMap<String, HashMap<String, Object>> myRank = new HashMap<>();
    Connector conn;
    FrameLayout btKelurahan, btKecamatan, btKota;
    TextView lblRank, lblNama, lblStatus, lblPoin;
    ImageView imgRank;
    PromptLoading pDialog;
    FrameLayout overlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_statistik);
        auth = new m_auth(this);
        conn = new Connector(this);

        overlay = findViewById(R.id.overlay);
        lblRank = this.findViewById(R.id.lblRank);
        lblNama = this.findViewById(R.id.lblNama);
        lblStatus = this.findViewById(R.id.lblStatus);
        lblPoin = this.findViewById(R.id.lblPoin);
        imgRank = this.findViewById(R.id.imgRank);

        btKelurahan = this.findViewById(R.id.btKelurahan);
        btKecamatan = this.findViewById(R.id.btKecamatan);
        btKota = this.findViewById(R.id.btKota);
        lvData = this.findViewById(R.id.lvData);


        overlay.setVisibility(View.VISIBLE);

        pDialog = new PromptLoading(this);
        pDialog.setMessage("Menghitung Statistik...");
        pDialog.setCancelable(false);
        pDialog.setOnTrueListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog.dismiss();
                finish();
                startActivity(getIntent());
            }
        });
        pDialog.setOnFalseListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog.dismiss();
                finish();
            }
        });
        pDialog.show();

        setData();

        layout = findViewById(R.id.layout);
        layout.setColorSchemeResources(R.color.mainPrimary, R.color.mainPrimary, android.R.color.white);
        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setData();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }

    public void showKelurahan(View v){
        btKota.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKecamatan.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKelurahan.setBackgroundColor(getResources().getColor(R.color.mainPrimaryDark));
        STATE = "kelurahan";
        setMyRank(myRank.get(STATE));
        adapter = new StatistikAdapter(Statistik.this, arrData.get(STATE));
        lvData.setAdapter(adapter);
    }
    public void showKecamatan(View v){
        btKota.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKelurahan.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKecamatan.setBackgroundColor(getResources().getColor(R.color.mainPrimaryDark));
        STATE = "kecamatan";
        setMyRank(myRank.get(STATE));
        adapter = new StatistikAdapter(Statistik.this, arrData.get(STATE));
        lvData.setAdapter(adapter);

    }
    public void showKota(View v){
        btKelurahan.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKecamatan.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        btKota.setBackgroundColor(getResources().getColor(R.color.mainPrimaryDark));
        STATE = "kota";
        setMyRank(myRank.get(STATE));
        adapter = new StatistikAdapter(Statistik.this, arrData.get(STATE));
        lvData.setAdapter(adapter);
    }

    private void setData(){

        arrData.clear();
        conn.statistik(new Connector.OnCompletedListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void OnCompleted(Object objData) {
                try {
                    final JSONObject obj = new JSONObject(objData.toString());
                    new AsyncTask<Boolean, Boolean, Boolean>() {

                        @Override
                        protected Boolean doInBackground(Boolean... params) {
                            try {
                                data = new ArrayList<>();
                                JSONArray arr = obj.getJSONArray("kelurahan");
                                for (int i=0; i<arr.length();i++){
                                    JSONObject itemObj = arr.getJSONObject(i);
                                    Iterator<String> iterator = itemObj.keys();
                                    item = new HashMap<>();
                                    while(iterator.hasNext()) {
                                        String currentKey = iterator.next();
                                        item.put(currentKey,itemObj.get(currentKey).toString());
                                    }
                                    data.add(item);
                                }
                                arrData.put("kelurahan",data);

                                data = new ArrayList<>();
                                arr = obj.getJSONArray("kecamatan");
                                for (int i=0; i<arr.length();i++){
                                    JSONObject itemObj = arr.getJSONObject(i);
                                    Iterator<String> iterator = itemObj.keys();
                                    item = new HashMap<>();
                                    while(iterator.hasNext()) {
                                        String currentKey = iterator.next();
                                        item.put(currentKey,itemObj.get(currentKey).toString());
                                    }
                                    data.add(item);
                                }
                                arrData.put("kecamatan",data);

                                data = new ArrayList<>();
                                arr = obj.getJSONArray("kota");
                                for (int i=0; i<arr.length();i++){
                                    JSONObject itemObj = arr.getJSONObject(i);
                                    Iterator<String> iterator = itemObj.keys();
                                    item = new HashMap<>();
                                    while(iterator.hasNext()) {
                                        String currentKey = iterator.next();
                                        item.put(currentKey,itemObj.get(currentKey).toString());
                                    }
                                    data.add(item);
                                }
                                arrData.put("kota",data);

                                JSONObject itemObj = obj.getJSONObject("mkelurahan");
                                Iterator<String> iterator = itemObj.keys();
                                item = new HashMap<>();
                                while(iterator.hasNext()) {
                                    String currentKey = iterator.next();
                                    item.put(currentKey,itemObj.get(currentKey).toString());
                                }
                                myRank.put("kelurahan",item);

                                itemObj = obj.getJSONObject("mkecamatan");
                                iterator = itemObj.keys();
                                item = new HashMap<>();
                                while(iterator.hasNext()) {
                                    String currentKey = iterator.next();
                                    item.put(currentKey,itemObj.get(currentKey).toString());
                                }
                                myRank.put("kecamatan",item);

                                itemObj = obj.getJSONObject("mkota");
                                iterator = itemObj.keys();
                                item = new HashMap<>();
                                while(iterator.hasNext()) {
                                    String currentKey = iterator.next();
                                    item.put(currentKey,itemObj.get(currentKey).toString());
                                }
                                myRank.put("kota",item);


                                return true;
                            } catch (JSONException e) {
                                Log.e("Connector JSON",e.getMessage());
                            }
                            return false;
                        }

                        @Override
                        protected void onPostExecute(Boolean result) {
                            super.onPostExecute(result);
                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            overlay.setVisibility(View.GONE);

                            setMyRank(myRank.get(STATE));
                            adapter = new StatistikAdapter(Statistik.this, arrData.get(STATE));
                            lvData.setAdapter(adapter);

                        }
                    }.execute();
                } catch (JSONException e) {
                    Log.e("CONVERT JSON",e.getMessage());
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    overlay.setVisibility(View.GONE);
                }
            }

            @Override
            public void OnFailed(int status) {
                adapter.notifyDataSetChanged();
                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
            }
        });
    }

    private void setMyRank(HashMap<String, Object> item){
        lblNama.setText(AppConfig.NullString(item.get("nama")));
        lblStatus.setText("W: "+AppConfig.NullString(item.get("menang")) + "\t\t\tD: "+AppConfig.NullString(item.get("seri")) + "\t\t\tL: "+AppConfig.NullString(item.get("kalah")));
        lblPoin.setText(AppConfig.number(AppConfig.NullNumber(item.get("poin"))));

        int position = AppConfig.NullNumber(item.get("rank"));
        if (position==1) {
            imgRank.setImageResource(R.drawable.ic_first);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position==2) {
            imgRank.setImageResource(R.drawable.ic_second);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position==3) {
            imgRank.setImageResource(R.drawable.ic_third);
            imgRank.setVisibility(View.VISIBLE);
            lblRank.setVisibility(View.GONE);
        }else if (position>3) {
            lblRank.setText(String.valueOf(position));
            lblRank.setVisibility(View.VISIBLE);
            imgRank.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }
}
