package fun.wolo.android.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fun.wolo.android.Launcher;
import fun.wolo.android.R;

public class Maintenance extends AppCompatActivity {
    String KET="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_maintenance);

        KET = getIntent().getStringExtra("KET");
        TextView lblKet = findViewById(R.id.lblKet);
        lblKet.setText(KET);

        LinearLayout btKeluar = findViewById(R.id.btKeluar);
        btKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Launcher.activityPaused();
                finishAffinity();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Launcher.activityPaused();
        finishAffinity();
    }
}
