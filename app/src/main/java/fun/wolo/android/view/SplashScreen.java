package fun.wolo.android.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.lib.ClockTimer;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptInfo;
import fun.wolo.android.model.m_auth;
import fun.wolo.android.view.authentication.Login;

public class SplashScreen extends AppCompatActivity {
    m_auth auth;
    Connector conn;
    PackageInfo pInfo = null;
    HashMap<String, Object> maintenance = new HashMap<>();
    ValueEventListener maintenanceListener;
    DatabaseReference firebaseDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        auth = new m_auth(this);
        conn = new Connector(this);
        firebaseDB = FirebaseDatabase.getInstance().getReference();

        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            conn.check(new Connector.OnCompletedListener() {
                @Override
                public void OnCompleted(Object data) {
                    if (AppConfig.NullNumber(data) > pInfo.versionCode){
                        final String appPackageName = getPackageName();
                        finish();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        }
                    }else{
                        run();
                    }
                }

                @Override
                public void OnFailed(int status) {
                    run();
                }
            });

        } catch (PackageManager.NameNotFoundException e) {
            Log.e("ERR VERSION","ERR");
            run();
        }
    }

    @Override
    protected void onStop() {
        if (maintenanceListener!=null){
            firebaseDB.child("maintenance").removeEventListener(maintenanceListener);
        }
        super.onStop();
    }

    private void run(){
        firebaseDB.keepSynced(false);
        maintenanceListener = firebaseDB.child("maintenance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrInfo = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    maintenance = new HashMap<>();
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot info = arrInfo.iterator().next();
                        maintenance.put(info.getKey(), info.getValue());
                    }
                    if (maintenance.size()>0){
                        if(maintenance.get("status").toString().equalsIgnoreCase("1") && maintenance.get("trial").toString().equalsIgnoreCase("0")){

                            Intent intent = new Intent(getApplicationContext(), Maintenance.class);
                            intent.putExtra("KET", AppConfig.NullString(maintenance.get("ket")).trim());
                            startActivity(intent);

                        }else{
                            start();
                        }
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR INFO",databaseError.getMessage());
                start();
            }
        });
    }


    private void start(){
        Launcher.clock = new ClockTimer(getBaseContext()).start(new ClockTimer.OnCompletedListener() {
            @Override
            public void OnCompleted() {
                if (auth.isLogged()) {
                    conn.token(new Connector.OnCompletedListener() {
                        @Override
                        public void OnCompleted(Object data) {
                            Intent pindah = new Intent(SplashScreen.this, Main.class);
                            startActivity(pindah);
                            SplashScreen.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();
                        }

                        @Override
                        public void OnFailed(int status) {
                            if (status == 0) {
                                Intent pindah = new Intent(SplashScreen.this, Main.class);
                                startActivity(pindah);
                                SplashScreen.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                finish();
                            } else {
                                PromptInfo dialog = new PromptInfo(SplashScreen.this, "Akun Anda telah dinon-aktifkan\nSilahkan hubungi Administrator WOLO FUN");
                                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        auth.logout();
                                        Intent intent = new Intent(SplashScreen.this, Login.class);
                                        startActivity(intent);
                                        SplashScreen.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                        finish();
                                    }
                                });
                                dialog.show();
                            }
                        }
                    });
                } else {
                    Thread timerThread = new Thread() {
                        public void run() {
                            try {
                                sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } finally {
                                Intent intent = new Intent(SplashScreen.this, Login.class);
                                startActivity(intent);
                                SplashScreen.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                finish();
                            }
                        }
                    };
                    timerThread.start();
                }
            }
        });
    }
}
