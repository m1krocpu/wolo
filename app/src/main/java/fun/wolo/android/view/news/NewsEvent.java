package fun.wolo.android.view.news;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewsEvent extends AppCompatActivity {
    WebView web;
    m_auth auth;
    SwipeRefreshLayout layout;
    String postData;
    TextView title;
    boolean clearHistory = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_news_event);
        auth = new m_auth(this);

        title = findViewById(R.id.title);
        web = this.findViewById(R.id.web);
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        web.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageFinished(WebView view, String url)
            {
                if (clearHistory)
                {
                    clearHistory = false;
                    web.clearHistory();
                }
                super.onPageFinished(view, url);
            }
        });
        postData = "access_token=" + auth.getAkun().akun_token;
        web.loadUrl(AppConfig.SERVER_API + "news?"+postData);

        layout = findViewById(R.id.layout);
        layout.setColorSchemeResources(R.color.mainPrimary, R.color.mainPrimary, android.R.color.white);
        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                web.reload();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setRefreshing(false);
                    }
                }, 2000);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    public void showNews(View v){
        title.setText("News");
        clearHistory = true;
        web.loadUrl(AppConfig.SERVER_API + "news?"+postData);
    }

    public void showEvent(View v){
        title.setText("Event");
        clearHistory = true;
        web.loadUrl(AppConfig.SERVER_API + "event?"+postData);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
            web.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }
}
