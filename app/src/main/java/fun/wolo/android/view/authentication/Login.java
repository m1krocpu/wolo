package fun.wolo.android.view.authentication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import fun.wolo.android.AppConfig;
import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptInfo;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Login extends AppCompatActivity {
    boolean PassState = false;
    ImageView btShowP;
    EditText etUSR, etPWD;
    TextView btDaftar;
    LinearLayout btLogin;
    Connector conn;
    m_auth auth;
    String USERNAME, PASSWORD;
    ProgressDialog pDialog;
    FrameLayout overlay;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Login.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_login);

        auth = new m_auth(this);
        conn = new Connector(this);

        overlay = findViewById(R.id.overlay);

        etUSR = findViewById(R.id.etUSR);
        etPWD = findViewById(R.id.etPWD);
        btShowP = findViewById(R.id.btShowP);
        btShowP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PassState){
                    PassState = true;
                    etPWD.setTransformationMethod(null);
                    btShowP.setImageResource(R.drawable.ic_password_unshow);
                }else{
                    PassState = false;
                    etPWD.setTransformationMethod(new PasswordTransformationMethod());
                    btShowP.setImageResource(R.drawable.ic_password_show);
                }
            }
        });
        btDaftar = findViewById(R.id.btDaftar);
        btDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Registrasi.class);
                startActivityForResult(intent,0);
            }
        });

        btLogin = findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etUSR.getText().toString().equalsIgnoreCase("")) {
                    View vKey = Login.this.getCurrentFocus();
                    if (vKey != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                    }

                    USERNAME = etUSR.getText().toString().trim();
                    PASSWORD = etPWD.getText().toString().trim();

                    overlay.setVisibility(View.VISIBLE);

                    pDialog = new ProgressDialog(Login.this);
                    pDialog.setMessage("Masuk Ke Akun Anda...");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);
                    pDialog.show();

                    conn.login(USERNAME, PASSWORD, new Connector.OnCompletedListener() {
                        @Override
                        public void OnCompleted(Object data) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            Intent pindah = new Intent(Login.this, AppConfig.MainApp.getClass());
                            startActivity(pindah);
                            finish();
                        }

                        @Override
                        public void OnFailed(int status) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            if (status==2) {
                                Toast.makeText(getWindow().getContext(), "Username atau Password Anda Salah", Toast.LENGTH_SHORT).show();
                            }else if (status==3){
                                new PromptInfo(Login.this, "Akun Anda telah dinon-aktifkan\nSilahkan hubungi Administrator WOLO FUN").show();
                            }
                        }
                    });
                }else {
                    Toast.makeText(getWindow().getContext(),"Username atau Password Tidak Boleh Kosong",Toast.LENGTH_SHORT).show();
                    etUSR.requestFocus();
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==1){
            finish();
        }
    }
}
