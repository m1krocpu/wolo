package fun.wolo.android.view.obrolan;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.adapter.ObrolanAdapter;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.Database;
import fun.wolo.android.model.m_auth;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ObrolanChat extends AppCompatActivity {
    String STATE = "publik", TITLE="publik";
    boolean STARTED = true;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    ArrayList<HashMap<String, Object>> chat = new ArrayList<>();
    Database db;
    m_auth auth;
    EmojiconEditText lblJawab;
    ImageView btEmoji, btSend;
    ListView lvData;
    EmojIconActions emojIcon;
    Calendar calendar;
    Connector conn;
    DatabaseReference firebaseDB;
    View rootView;
    ObrolanAdapter adapter;
    ValueEventListener listener;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obrolan_chat);
        db = new Database(this);
        auth = new m_auth(this);
        conn = new Connector(this);
        calendar = Calendar.getInstance();
        firebaseDB = FirebaseDatabase.getInstance().getReference();

        STATE = getIntent().getStringExtra("STATE");
        TITLE = getIntent().getStringExtra("TITLE");


        firebaseDB.child("obrolan").child("audiensi").child(STATE).child(auth.getAkun().akun_clientsecret).setValue(true);

        title = findViewById(R.id.title);
        title.setText(TITLE.toUpperCase());
        rootView = findViewById(R.id.rootView);
        lvData = findViewById(R.id.lvData);
        adapter = new ObrolanAdapter(this, chat);
        lvData.setAdapter(adapter);

        lblJawab = findViewById(R.id.lblJawab);
        btEmoji = findViewById(R.id.btEmoji);
        emojIcon=new EmojIconActions(this,rootView,lblJawab,btEmoji);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {

            }
        });


        btSend = findViewById(R.id.btSend);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View vKey = ObrolanChat.this.getCurrentFocus();
                if (vKey != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                }

                if (!lblJawab.getText().toString().trim().equalsIgnoreCase("")){
                    String JAWAB = lblJawab.getText().toString().trim();

                    HashMap<String,Object> item = new HashMap<>();
                    item.put("usr", auth.getAkun().akun_nama);
                    item.put("msg",JAWAB);
                    item.put("time", AppConfig.getTime());
                    chat.add(item);
                    adapter.notifyDataSetChanged();
                    scrollListViewToBottom();

                    conn.setObrolan(STATE, JAWAB);

                    lblJawab.setText("");
                }
            }
        });

        listener = firebaseDB.child("obrolan").child(STATE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chat.clear();
                Iterable<DataSnapshot> arrJawab = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Jawab = arrJawab.iterator().next();
                        HashMap<String, Object> itemChat = new HashMap<>();
                        if (Jawab.getChildrenCount()>0) {
                            Iterable<DataSnapshot> column = Jawab.getChildren();
                            for (int j=0; j<Jawab.getChildrenCount();j++){
                                DataSnapshot data = column.iterator().next();
                                itemChat.put(data.getKey(), data.getValue());

                            }
                            chat.add(itemChat);
                            adapter.notifyDataSetChanged();

                            if (STARTED){
                                scrollListViewToBottom();
                                STARTED = false;
                            }
                        }
                    }
                }
            }@Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR CHAT",databaseError.getMessage());
            }
        });
    }
    private void scrollListViewToBottom() {
        lvData.post(new Runnable() {
            @Override
            public void run() {
                lvData.setSelection(adapter.getCount() - 1);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void back(View v){
        finish();
        firebaseDB.child("obrolan").child(STATE).removeEventListener(listener);
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        firebaseDB.child("obrolan").child(STATE).removeEventListener(listener);
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }
}
