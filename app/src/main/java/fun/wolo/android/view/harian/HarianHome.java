package fun.wolo.android.view.harian;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.adapter.KuisAdapter;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.Database;
import fun.wolo.android.model.m_auth;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HarianHome extends AppCompatActivity {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Database db;
    m_auth auth;
    String ACTIVEID="-";

    LinearLayout panelTimer, panelPeople, panelReward, panelEnd;
    TextView lblSoal, btStatistik, lblEnd, lblTimer, lblPeople, lblReward, lblEndInfo;
    EmojiconEditText lblJawab;
    ImageView btEmoji, btSend;
    ListView lvData;
    FrameLayout footer;
    String REWARD;
    Date date1, date2;
    Connector conn;
    DatabaseReference firebaseDB;
    ArrayList<HashMap<String, Object>> data = new ArrayList<>();
    ArrayList<HashMap<String, Object>> chat = new ArrayList<>();
    KuisAdapter adapter;
    EmojIconActions emojIcon;
    View rootView;
    boolean waiting = false, progress=false, finish=false;

    ValueEventListener lastListener;
    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_harian_home);
        db = new Database(this);
        auth = new m_auth(this);
        conn = new Connector(this);
        firebaseDB = FirebaseDatabase.getInstance().getReference();

        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.cancelAll();

        rootView = findViewById(R.id.rootView);
        panelTimer = findViewById(R.id.panelTimer);
        panelPeople = findViewById(R.id.panelPeople);
        panelReward = findViewById(R.id.panelReward);
        panelEnd = findViewById(R.id.panelEnd);
        lblEnd = findViewById(R.id.lblEnd);
        lblTimer = findViewById(R.id.lblTimer);
        lblPeople = findViewById(R.id.lblPeople);
        lblReward = findViewById(R.id.lblReward);
        lblEndInfo = findViewById(R.id.lblEndInfo);
        lblSoal = findViewById(R.id.lblSoal);
        btStatistik = findViewById(R.id.btStatistik);
        lvData = findViewById(R.id.lvData);
        footer = findViewById(R.id.footer);

        lblJawab = findViewById(R.id.lblJawab);
        btEmoji = findViewById(R.id.btEmoji);
        emojIcon=new EmojIconActions(this,rootView,lblJawab,btEmoji);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {

            }

            @Override
            public void onKeyboardClose() {

            }
        });

        btSend = findViewById(R.id.btSend);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View vKey = HarianHome.this.getCurrentFocus();
                if (vKey != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                }

                if (!lblJawab.getText().toString().trim().equalsIgnoreCase("")){
                    String JAWAB = lblJawab.getText().toString().trim();

                    HashMap<String,Object> item = new HashMap<>();
                    item.put("usr", auth.getAkun().akun_nama);
                    item.put("msg",JAWAB);
                    item.put("time",AppConfig.getTime());
                    chat.add(item);
                    adapter.notifyDataSetChanged();


                    conn.setKuis(Launcher.IDKUIS, JAWAB);

                    lblJawab.setText("");
                }
            }
        });

        adapter = new KuisAdapter(this,chat);
        lvData.setAdapter(adapter);

        ACTIVEID = Launcher.IDKUIS;
        String TGL = auth.getAkun().akun_tanggal;
        if (Launcher.clock.getDateString()!=null){
            TGL = Launcher.clock.getDateString();
        }

        db.Open();
        data = db.getKuis(TGL);
        if (data.size()<=0){
            lblEndInfo.setText("Maaf, Hari ini tidak Ada Kuis Harian ");
            lblEnd.setText("");
        }

        long millis = 24 * 60 * 1000;
        final CountDownTimer timer = new CountDownTimer(millis, 1000) {
            public void onTick(long millisUntilFinished) {
                if (!ACTIVEID.equalsIgnoreCase("")){
                    if (!ACTIVEID.equalsIgnoreCase(Launcher.IDKUIS)){
                        cancel();
                        finish();
                        startActivity(getIntent());
                    }
                }
                if (Launcher.NOWKUIS.size()>0){
                    setTimer(Launcher.NOWKUIS);
                }else{
                    lblEndInfo.setText("Mohon Menunggu...");
                    lblEnd.setText("");
                    lblSoal.setText("");
                }
            }

            public void onFinish() {

            }
        }.start();

//        db.Open();
//        data = db.getKuis(Launcher.clock.getDateString());
//        if (data.size()>0){
//            setTimer(data.get(index));
//        }else{
//            lblEndInfo.setText("Maaf, Hari ini tidak Ada Kuis Harian ");
//            lblEnd.setText("");
//        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    private void setTimer(HashMap<String, Object> dataKuis){
        REWARD = dataKuis.get("hadiah").toString();
        try {
            date1 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("mulai").toString());
            date2 = df.parse(Launcher.clock.getDateString() + " " + dataKuis.get("selesai").toString());

            if (date1.getTime() - Launcher.clock.getDate().getTime() >=0 ){
                if (!waiting){
                    showWaiting();
                    waiting = true;
                    progress = false;
                    finish = false;
                }

                long millis = date1.getTime() - Launcher.clock.getDate().getTime();
                int seconds = (int) (millis / 1000) % 60 ;
                int minutes = (int) ((millis / (1000*60)) % 60);
                int hours   = (int) ((millis / (1000*60*60)) % 24);

                lblEnd.setText((hours <10 ? "0"+hours:hours) + ":" + (minutes <10 ? "0"+minutes:minutes) + ":" + (seconds <10 ? "0"+seconds:seconds));
            }else{
                if (date2.getTime() - Launcher.clock.getDate().getTime()>0){
                    if (!progress){
                        showProgress();
                        progress = true;
                        waiting = false;
                        finish = false;
                    }
                    long millis = date2.getTime() - Launcher.clock.getDate().getTime();
                    int seconds = (int) (millis / 1000) % 60 ;
                    int minutes = (int) ((millis / (1000*60)) % 60);

                    lblTimer.setText(minutes + " Menit " + seconds + " Detik");
                }else{
                    if (!finish){
                        showFinish();
                        finish = true;
                        waiting = false;
                        progress = false;
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void showWaiting(){
        panelTimer.setVisibility(View.GONE);
        panelPeople.setVisibility(View.GONE);
        panelReward.setVisibility(View.GONE);
        panelEnd.setVisibility(View.VISIBLE);
        lblSoal.setTextColor(getResources().getColor(android.R.color.black));
        btStatistik.setVisibility(View.GONE);
        footer.setVisibility(View.GONE);
        showLasted();
        lblEndInfo.setText("Kuis Berikutnya dalam ");

//        new CountDownTimer(date1.getTime() - Launcher.clock.getDate().getTime(), 1000) {
//            @SuppressLint("SetTextI18n")
//            public void onTick(long millisUntilFinished) {
//                int seconds = (int) (millisUntilFinished / 1000) % 60 ;
//                int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
//                int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);
//
//                lblEnd.setText((hours <10 ? "0"+hours:hours) + ":" + (minutes <10 ? "0"+minutes:minutes) + ":" + (seconds <10 ? "0"+seconds:seconds));
//            }
//
//            public void onFinish() {
//                finish();
//                startActivity(getIntent());
//            }
//        }.start();
    }

    private void showProgress(){

        if (lastListener!=null) {
            firebaseDB.child("kuis").child("jawab").child(Launcher.LASTIDKUIS).removeEventListener(lastListener);
        }

        if (!Launcher.LASTIDKUIS.trim().equalsIgnoreCase("")){
            firebaseDB.child("kuis").child("jawab").child(Launcher.LASTIDKUIS).removeValue();
        }

        firebaseDB.child("kuis").child("soal").child(Launcher.IDKUIS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblSoal.setText(AppConfig.NullString(dataSnapshot.getValue()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                lblSoal.setText("");
            }
        });
        chat.clear();
        lblPeople.setText("0");
        lblReward.setText("Rp " + AppConfig.number(AppConfig.NullNumber(REWARD)));
        panelTimer.setVisibility(View.VISIBLE);
        panelPeople.setVisibility(View.VISIBLE);
        panelReward.setVisibility(View.VISIBLE);
        panelEnd.setVisibility(View.GONE);
        lblSoal.setTextColor(getResources().getColor(android.R.color.black));
        btStatistik.setVisibility(View.GONE);
        footer.setVisibility(View.VISIBLE);


//        new CountDownTimer(date2.getTime() - Launcher.clock.getDate().getTime(), 1000) {
//            public void onTick(long millisUntilFinished) {
//                int seconds = (int) (millisUntilFinished / 1000) % 60 ;
//                int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
//
//                lblTimer.setText(minutes + " Menit " + seconds + " Detik");
//            }
//
//            public void onFinish() {
//                //showFinish();
//                finish();
//                startActivity(getIntent());
//            }
//        }.start();

        firebaseDB.child("kuis").child("audiensi").child(Launcher.IDKUIS).child(auth.getAkun().akun_clientsecret).setValue(true);
        firebaseDB.child("kuis").child("audiensi").child(Launcher.IDKUIS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblPeople.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR AUDIENSI",databaseError.getMessage());
            }
        });
        firebaseDB.child("kuis").child("jawab").child(Launcher.IDKUIS).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chat.clear();
                Iterable<DataSnapshot> arrJawab = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Jawab = arrJawab.iterator().next();
                        HashMap<String, Object> itemChat = new HashMap<>();
                        if (Jawab.getChildrenCount()>0) {
                            Iterable<DataSnapshot> column = Jawab.getChildren();
                            for (int j=0; j<Jawab.getChildrenCount();j++){
                                DataSnapshot data = column.iterator().next();
                                itemChat.put(data.getKey(), data.getValue());

                            }
                            if (itemChat.size()>0) {
                                chat.add(itemChat);
                            }
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR JAWAB",databaseError.getMessage());
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }


    private void showFinish(){
        panelTimer.setVisibility(View.GONE);
        panelPeople.setVisibility(View.GONE);
        panelReward.setVisibility(View.GONE);
        panelEnd.setVisibility(View.VISIBLE);
        footer.setVisibility(View.GONE);

        lblSoal.setTextColor(getResources().getColor(R.color.mainPrimary));
        btStatistik.setVisibility(View.GONE);

        if (Launcher.KUISSELESAI){
            lblEndInfo.setText("Kuis Harian telah ");
            lblEnd.setText("SELESAI");

            showLasted();
        }
    }

    private void showLasted(){
        if (!Launcher.LASTIDKUIS.trim().equalsIgnoreCase("")){
            lblSoal.setTextColor(getResources().getColor(R.color.mainPrimary));
            conn.getKuisWinner(Launcher.LASTIDKUIS, new Connector.OnCompletedListener() {
                @Override
                public void OnCompleted(Object data) {
                    lblSoal.setText(data.toString());
                }

                @Override
                public void OnFailed(int status) {

                }
            });

            firebaseDB.child("kuis").child("winner").child(Launcher.LASTIDKUIS).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    lblSoal.setText(AppConfig.NullString(dataSnapshot.getValue()));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    lblSoal.setText("");
                }
            });

            lastListener = firebaseDB.child("kuis").child("jawab").child(Launcher.LASTIDKUIS).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    chat.clear();
                    Iterable<DataSnapshot> arrJawab = dataSnapshot.getChildren();
                    if (dataSnapshot.getChildrenCount()>0) {
                        for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                            DataSnapshot Jawab = arrJawab.iterator().next();
                            HashMap<String, Object> itemChat = new HashMap<>();
                            if (Jawab.getChildrenCount()>0) {
                                Iterable<DataSnapshot> column = Jawab.getChildren();
                                for (int j=0; j<Jawab.getChildrenCount();j++){
                                    DataSnapshot data = column.iterator().next();
                                    itemChat.put(data.getKey(), data.getValue());

                                }
                                chat.add(itemChat);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("ERROR JAWAB",databaseError.getMessage());
                }
            });
        }
    }
}
