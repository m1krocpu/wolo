package fun.wolo.android.view.authentication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Registrasi extends AppCompatActivity {
    TextView btLogin;
    LinearLayout btRegistrasi;
    Connector conn;
    EditText etNIK, etHP;
    String NIK, HP;
    FrameLayout overlay;
    ProgressDialog pDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        conn = new Connector(this);

        overlay = findViewById(R.id.overlay);
        etNIK = findViewById(R.id.etNIK);
        etHP = findViewById(R.id.etHP);

        btLogin = findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btRegistrasi = findViewById(R.id.btRegistrasi);
        btRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etNIK.getText().toString().trim().equalsIgnoreCase("") && !etHP.getText().toString().trim().equalsIgnoreCase("")){
                    View vKey = Registrasi.this.getCurrentFocus();
                    if (vKey != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                    }
                    NIK = etNIK.getText().toString().trim();
                    HP = etHP.getText().toString().trim();

                    overlay.setVisibility(View.VISIBLE);

                    pDialog = new ProgressDialog(Registrasi.this);
                    pDialog.setMessage("Mengirim Verifikasi...");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);
                    pDialog.show();

                    conn.reg_nik(NIK, HP, new Connector.OnCompletedListener() {
                        @Override
                        public void OnCompleted(Object data) {
                            if (pDialog.isShowing()){
                                pDialog.dismiss();
                            }
                            overlay.setVisibility(View.GONE);
                            Intent intent = new Intent(Registrasi.this, RegistrasiStatus.class);
                            intent.putExtra("NIK",NIK);
                            startActivityForResult(intent,0);
                        }

                        @Override
                        public void OnFailed(int status) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            if (status==3){
                                //Toast.makeText(Registrasi.this, "NIK tidak terdaftar!",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Registrasi.this, RegistrasiManual.class);
                                intent.putExtra("NIK",NIK);
                                intent.putExtra("HP",HP);
                                startActivityForResult(intent,0);
                            }else if (status==2){
                                Toast.makeText(Registrasi.this, "NIK telah terdaftar!",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    Toast.makeText(Registrasi.this, "isikan NIK dan No HP dengan benar!",Toast.LENGTH_LONG).show();
                    etNIK.requestFocus();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==1){
            setResult(1);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(0);
        super.onBackPressed();
    }
}
