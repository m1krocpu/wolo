package fun.wolo.android.view.authentication;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegistrasiManual extends AppCompatActivity {
    LinearLayout btRegistrasi;
    String NIK, HP, NAMA, DESA="";
    TextView lblNIK;
    FrameLayout overlay;
    ProgressDialog pDialog;
    Connector conn;

    EditText etNIK, etNama, etHP;
    AutoCompleteTextView etDesa;

    ArrayList<HashMap<String, Object>> data = new ArrayList<>();
    ArrayList<String> arrDesa = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi_manual);
        NIK = getIntent().getStringExtra("NIK");
        HP = getIntent().getStringExtra("HP");
        conn = new Connector(this);

        overlay = findViewById(R.id.overlay);

        lblNIK = findViewById(R.id.lblNIK);
        lblNIK.setText("NIK : " + NIK);


        etNIK = findViewById(R.id.etNIK);
        etNama = findViewById(R.id.etNama);
        etDesa = findViewById(R.id.etDesa);
        etHP = findViewById(R.id.etHP);

        etNIK.setText(NIK);
        etHP.setText(HP);

        btRegistrasi = findViewById(R.id.btRegistrasi);
        btRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View vKey = RegistrasiManual.this.getCurrentFocus();
                if (vKey != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
                }

                NIK = etNIK.getText().toString().trim();
                NAMA = etNama.getText().toString().trim();
                HP = etHP.getText().toString().trim();
                DESA = etDesa.getText().toString().trim();

                if (NIK.equalsIgnoreCase("")){
                    Toast.makeText(RegistrasiManual.this,"NIK belum diisi!", Toast.LENGTH_LONG).show();
                }else if (NAMA.equalsIgnoreCase("")){
                    Toast.makeText(RegistrasiManual.this,"Nama Lengkap belum diisi!", Toast.LENGTH_LONG).show();
                }else if (HP.equalsIgnoreCase("")){
                    Toast.makeText(RegistrasiManual.this,"HP belum diisi!", Toast.LENGTH_LONG).show();
                }else if (DESA.equalsIgnoreCase("")){
                    Toast.makeText(RegistrasiManual.this,"DESA belum diisi!", Toast.LENGTH_LONG).show();
                }else{
                    overlay.setVisibility(View.VISIBLE);

                    pDialog = new ProgressDialog(RegistrasiManual.this);
                    pDialog.setMessage("Mengirim Data...");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(false);
                    pDialog.show();

                    conn.reg_manual(NIK, HP, NAMA, DESA, new Connector.OnCompletedListener() {
                        @Override
                        public void OnCompleted(Object data) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            Intent intent = new Intent(RegistrasiManual.this, RegistrasiStatus.class);
                            intent.putExtra("NIK",NIK);
                            startActivityForResult(intent,0);
                        }

                        @Override
                        public void OnFailed(int status) {
                            pDialog.dismiss();
                            overlay.setVisibility(View.GONE);
                            if (status==2){
                                Toast.makeText(RegistrasiManual.this, "NIK telah terdaftar!",Toast.LENGTH_LONG).show();
                            }
                            else if (status==3){
                                Toast.makeText(RegistrasiManual.this, "Desa/Kelurahan tidak ditemukan!",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
        overlay.setVisibility(View.VISIBLE);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mohon Menunggu...");
        pDialog.setCancelable(false);
        pDialog.show();

        conn.getDesa(new Connector.OnCompletedListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void OnCompleted(final Object response) {
                data.clear();
                arrDesa.clear();
                try {
                    final JSONArray array = new JSONArray(response.toString());
                    new AsyncTask<Integer, Integer, Integer>() {

                        @Override
                        protected Integer doInBackground(Integer... params) {
                            if(array.length()>0){
                                for(int i=0;i<array.length();i++){
                                    try {
                                        JSONObject obj = array.getJSONObject(i);
                                        HashMap<String, Object> item = new HashMap<>();
                                        item.put("id", obj.getString("desa_id"));
                                        item.put("nama", obj.getString("desa_nama"));
                                        data.add(item);
                                        arrDesa.add(obj.getString("desa_nama"));
                                    } catch (JSONException e) {
                                        Log.e("Connector JSON",e.getMessage());
                                    }
                                }
                            }
                            return 0;
                        }

                        @Override
                        protected void onPostExecute(Integer result) {
                            super.onPostExecute(result);
                            overlay.setVisibility(View.GONE);
                            pDialog.dismiss();

                            ArrayAdapter adapter = new ArrayAdapter(RegistrasiManual.this, android.R.layout.simple_list_item_1, arrDesa);

                            etDesa.setAdapter(adapter);
                            etDesa.setThreshold(1);

                        }
                    }.execute();
                } catch (JSONException e) {
                    overlay.setVisibility(View.GONE);
                    pDialog.dismiss();
                }



            }

            @Override
            public void OnFailed(int status) {
                overlay.setVisibility(View.GONE);
                pDialog.dismiss();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onBackPressed() {
        setResult(0);
        super.onBackPressed();
    }
}
