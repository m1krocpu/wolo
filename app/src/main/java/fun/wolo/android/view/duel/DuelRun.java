package fun.wolo.android.view.duel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.HashMap;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptInfo;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DuelRun extends AppCompatActivity {
    boolean FINISHED = false;
    m_auth auth;
    int kategori = 1, retry = 0, sALLY=0, sENEMY=0;
    String[] arrKategori = {"BAHASA","SEJARAH","PENDIDIKAN","LOGIKA","UMUM"};
    Integer[] col = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    Integer[] SCORE = {0,0,0,0,0};
    String ID, ROOM, ALLY, ENEMY, lALLY, lENEMY;
    Connector conn;
    TextView lblTimer;
    DatabaseReference firebaseDB;
    TextView nickAlly, lokasiAlly, nickEnemy, lokasiEnemy,  lblSoal, lblKategori, scoreAlly, scoreEnemy, trophy_text, btJawab, lblHasil, lblRonde, lblState, lblTopTimer;
    TextView soal1, soal2, soal3, soal4, soal5;
    LinearLayout layoutJawab, layoutTimer, layoutSelesai;

    int[] resJawab = new int[]{
            R.id.j11, R.id.j12, R.id.j13, R.id.j14, R.id.j15, R.id.j16, R.id.j17, R.id.j18,
            R.id.j21, R.id.j22, R.id.j23, R.id.j24, R.id.j25, R.id.j26, R.id.j27, R.id.j28,
            R.id.j31, R.id.j32, R.id.j33, R.id.j34, R.id.j35, R.id.j36, R.id.j37, R.id.j38
    };
    EditText[] etJawab = new EditText[resJawab.length];
    int lastKategori=1;


    CountDownTimer timer;
    ValueEventListener mListener, soalListener, hasilListener;
    ProgressDialog pDialog;
    FrameLayout overlay;
    CallbackManager callbackManager;
    AssetManager am;
    MediaPlayer player;

    int stateALLY=1, stateENEMY=1;
    HashMap<String, Object> soal = new HashMap<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_duel_run);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        if (FINISHED){
            finish();
            this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
        }

        //AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //am.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);
        player = MediaPlayer.create(this, R.raw.beep_01);

        auth = new m_auth(this);
        conn = new Connector(this);
        ROOM = getIntent().getStringExtra("ROOM");
        ALLY = getIntent().getStringExtra("nALLY");
        ENEMY = getIntent().getStringExtra("nENEMY");
        lALLY = getIntent().getStringExtra("lALLY");
        lENEMY = getIntent().getStringExtra("lENEMY");

        lblState = findViewById(R.id.lblState);
        overlay = findViewById(R.id.overlay);
        lblRonde = findViewById(R.id.lblRonde);
        lblHasil = findViewById(R.id.lblHasil);
        lblTimer = findViewById(R.id.lblTimer);
        lblTopTimer = findViewById(R.id.lblTopTimer);
        nickAlly = findViewById(R.id.nickAlly);
        lokasiAlly = findViewById(R.id.lokasiAlly);
        nickEnemy = findViewById(R.id.nickEnemy);
        lokasiEnemy = findViewById(R.id.lokasiEnemy);
        scoreAlly = findViewById(R.id.scoreAlly);
        scoreEnemy = findViewById(R.id.scoreEnemy);
        lblSoal = findViewById(R.id.lblSoal);
        lblKategori = findViewById(R.id.lblKategori);
        layoutJawab = findViewById(R.id.layoutJawab);
        layoutTimer = findViewById(R.id.layoutTimer);
        layoutSelesai = findViewById(R.id.layoutSelesai);
        trophy_text = findViewById(R.id.trophy_text);
        btJawab = findViewById(R.id.btJawab);

        soal1 = findViewById(R.id.soal1);
        soal2 = findViewById(R.id.soal2);
        soal3 = findViewById(R.id.soal3);
        soal4 = findViewById(R.id.soal4);
        soal5 = findViewById(R.id.soal5);

        for(int i=0;i<resJawab.length;i++){
            etJawab[i] = findViewById(resJawab[i]);
        }

        scoreAlly.setText("0");
        scoreEnemy.setText("0");
        nickAlly.setText("@"+ALLY);
        nickEnemy.setText("@"+ENEMY);
        lokasiAlly.setText(lALLY);
        lokasiEnemy.setText(lENEMY);

        firebaseDB = FirebaseDatabase.getInstance().getReference();
        mListener = firebaseDB.child("duel").child("run").child(ROOM).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Room = arrRoom.iterator().next();
                        if (Room.getKey().equalsIgnoreCase(ALLY)){
                            sALLY = AppConfig.NullNumber(Room.getValue());
                            scoreAlly.setText(Room.getValue().toString());
                        }else if (Room.getKey().equalsIgnoreCase(ENEMY)){
                            sENEMY = AppConfig.NullNumber(Room.getValue());
                            scoreEnemy.setText(Room.getValue().toString());
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR DUEL RUN",databaseError.getMessage());
            }
        });

        setFormJawab();

        run();
    }

    public void back(View v){
        if (mListener!=null){
            firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(mListener);
        }
        if (hasilListener!=null){
            firebaseDB.child("duel").child("state").child(ROOM).removeEventListener(hasilListener);
        }
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    private int countScore(){
        int tmp=0;
        for (int i:SCORE) {
            if (i==1){
                tmp++;
            }
        }
        return tmp;
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    public void submitJawaban(View v){
        lastKategori = kategori;
        View vKey = DuelRun.this.getCurrentFocus();
        if (vKey != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(vKey.getWindowToken(), 0);
        }
        final String JAWABAN = getJawaban();
        if (!JAWABAN.trim().equalsIgnoreCase("")){
             layoutJawab.setVisibility(View.INVISIBLE);
             conn.jawabDuel(ROOM, ID, JAWABAN, new Connector.OnCompletedListener() {
                @Override
                public void OnCompleted(Object data) {
                    if (lastKategori==kategori) {
                        if (ALLY.equalsIgnoreCase(auth.getAkun().akun_nama)){
                            SCORE[lastKategori-1] = 1;
                            scoreAlly.setText(String.valueOf(countScore()));
                        }else{
                            SCORE[lastKategori-1] = 1;
                            scoreEnemy.setText(String.valueOf(countScore()));
                        }
                        layoutJawab.setVisibility(View.GONE);
                        lblHasil.setText("\"" + JAWABAN + "\"\n" + "Jawaban BENAR");
                        lblHasil.setTextColor(getResources().getColor(R.color.green));
                        lblHasil.setVisibility(View.VISIBLE);
                    }

                    if (lastKategori==1){
                        soal1.setBackgroundResource(R.drawable.green_cyrcle);
                    }else  if (lastKategori==2){
                        soal2.setBackgroundResource(R.drawable.green_cyrcle);
                    }else  if (lastKategori==3){
                        soal3.setBackgroundResource(R.drawable.green_cyrcle);
                    }else  if (lastKategori==4){
                        soal4.setBackgroundResource(R.drawable.green_cyrcle);
                    }else{
                        soal5.setBackgroundResource(R.drawable.green_cyrcle);
                    }
                }

                @Override
                public void OnFailed(int status) {
                    if (lastKategori==kategori) {
                        layoutJawab.setVisibility(View.GONE);
                        lblHasil.setText("\"" + JAWABAN + "\"\n" + "Jawaban SALAH");
                        lblHasil.setTextColor(getResources().getColor(R.color.mainPrimary));
                        if (kategori<6) {
                            lblHasil.setVisibility(View.VISIBLE);
                        }
                    }

                    if (lastKategori==1){
                        soal1.setBackgroundResource(R.drawable.red_cyrcle);
                    }else  if (lastKategori==2){
                        soal2.setBackgroundResource(R.drawable.red_cyrcle);
                    }else  if (lastKategori==3){
                        soal3.setBackgroundResource(R.drawable.red_cyrcle);
                    }else  if (lastKategori==4){
                        soal4.setBackgroundResource(R.drawable.red_cyrcle);
                    }else{
                        soal5.setBackgroundResource(R.drawable.red_cyrcle);
                    }
                }
            });
        }
    }

    public void mainLagi(View v){
        if (mListener!=null){
            firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(mListener);
        }
        if (hasilListener!=null){
            firebaseDB.child("duel").child("state").child(ROOM).removeEventListener(hasilListener);
        }

        overlay.setVisibility(View.VISIBLE);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Membuat Duel Kuis...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        conn.setDuel(new Connector.OnCompletedListener() {
            @Override
            public void OnCompleted(Object data) {
                auth.setLastRoom(data.toString());

                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                Intent intent = new Intent(DuelRun.this, DuelBuat.class);
                intent.putExtra("STATE","CREATE");
                intent.putExtra("ROOM",data.toString());
                startActivity(intent);
                finish();
                DuelRun.this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
            }

            @Override
            public void OnFailed(int status) {
                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                if (status==1){
                    new PromptInfo(DuelRun.this,"Duel Kuis gagal dibuat, silahkan coba lagi!").show();
                }
            }
        });
    }

    public void bagikan(View v){
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn){
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(createShare())
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            ShareDialog shareDialog = new ShareDialog(DuelRun.this);
            shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
        }else{
            LoginManager loginManager = LoginManager.getInstance();
            callbackManager = CallbackManager.Factory.create();
            loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(createShare())
                            .build();
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();
                    ShareDialog shareDialog = new ShareDialog(DuelRun.this);
                    shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {
                }
            });
            loginManager.logInWithPublishPermissions(DuelRun.this, Arrays.asList("publish_actions"));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(callbackManager!=null){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private Bitmap createShare() {
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        RelativeLayout view = new RelativeLayout(this);
        mInflater.inflate(R.layout.share_duel, view, true);
        ImageView imgBackground = view.findViewById(R.id.imgBackground);
        TextView nAlly = view.findViewById(R.id.nickAlly);
        TextView lAlly = view.findViewById(R.id.lokasiAlly);
        TextView nEnemy = view.findViewById(R.id.nickEnemy);
        TextView lEnemy = view.findViewById(R.id.lokasiEnemy);
        TextView s1 = view.findViewById(R.id.scoreAlly);
        TextView s2 = view.findViewById(R.id.scoreEnemy);

        nAlly.setText("@"+ALLY);
        lAlly.setText("( "+lALLY + " )");
        nEnemy.setText("@"+ENEMY);
        lEnemy.setText("( "+lENEMY + " )");
        s1.setText(String.valueOf(sALLY));
        s2.setText(String.valueOf(sENEMY));
        if (sALLY>sENEMY){
            if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                imgBackground.setImageResource(R.drawable.duel_menang);
            }else{
                imgBackground.setImageResource(R.drawable.duel_kalah);
            }
        }else if (sALLY==sENEMY){
            imgBackground.setImageResource(R.drawable.duel_seri);
        }else{
            if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                imgBackground.setImageResource(R.drawable.duel_kalah);
            }else{
                imgBackground.setImageResource(R.drawable.duel_menang);
            }
        }

        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);

        view.draw(c);
        return bitmap;
    }

    private String getJawaban(){
        StringBuilder JAWABAN= new StringBuilder();
        for(int i=0;i<resJawab.length;i++){
            JAWABAN.append(AppConfig.NullString(etJawab[i].getText().toString()));
        }
        return JAWABAN.toString();
    }

    private void run(){
        if (timer!=null){
            timer.cancel();
        }
        firebaseDB.child("duel").child("state").child(ROOM).child(auth.getAkun().akun_nama).setValue(kategori);
        layoutJawab.setVisibility(View.VISIBLE);
        lblHasil.setVisibility(View.GONE);
        btJawab.setVisibility(View.VISIBLE);
        lblTimer.setText("15 Detik");
        lblTopTimer.setText("0");
        lblKategori.setText(arrKategori[kategori-1]);
        if (kategori==1){
            soal1.setBackgroundResource(R.drawable.yellow_cyrcle);
        }else  if (kategori==2){
            soal2.setBackgroundResource(R.drawable.yellow_cyrcle);
        }else  if (kategori==3){
            soal3.setBackgroundResource(R.drawable.yellow_cyrcle);
        }else  if (kategori==4){
            soal4.setBackgroundResource(R.drawable.yellow_cyrcle);
        }else{
            soal5.setBackgroundResource(R.drawable.yellow_cyrcle);
        }
        lblRonde.setText("RONDE : " + kategori);
        soal = Launcher.DUELSOAL.get(String.valueOf(kategori));
        if (soal != null){
            lblSoal.setText(Html.fromHtml(AppConfig.NullString(soal.get("soal"))));
            setJawab(AppConfig.NullNumber(soal.get("num")), AppConfig.NullNumber(soal.get("space")));
            ID = soal.get("uid").toString();
        }
        timer = new CountDownTimer(25 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000) % 60;
                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);

                if (minutes > 0) {
                    lblTimer.setText(minutes + " Menit " + seconds + " Detik");
                } else {
                    if (seconds<=5){
                        if (player!=null) {
                            player.start();
                        }
                    }
                    lblTimer.setText(seconds + " Detik");
                    lblTopTimer.setText(seconds+"");
                }
            }

            public void onFinish() {
                lblTopTimer.setText("0");
                kategori++;
                if (kategori<6){
                    run();
                }else{
                    firebaseDB.child("duel").child("state").child(ROOM).child(auth.getAkun().akun_nama).setValue(kategori);
                    FINISHED = true;
                    lblSoal.setText("DUEL TELAH BERAKHIR");
                    lblKategori.setVisibility(View.INVISIBLE);
                    layoutJawab.setVisibility(View.GONE);
                    lblHasil.setVisibility(View.GONE);
                    if(stateALLY==6 && stateENEMY==6){
                        mListener = firebaseDB.child("duel").child("run").child(ROOM).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                                if (dataSnapshot.getChildrenCount()>0) {
                                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                                        DataSnapshot Room = arrRoom.iterator().next();
                                        if (Room.getKey().equalsIgnoreCase(ALLY)){
                                            sALLY = AppConfig.NullNumber(Room.getValue());
                                            scoreAlly.setText(Room.getValue().toString());
                                        }else if (Room.getKey().equalsIgnoreCase(ENEMY)){
                                            sENEMY = AppConfig.NullNumber(Room.getValue());
                                            scoreEnemy.setText(Room.getValue().toString());
                                        }
                                    }
                                    lblHasil.setVisibility(View.GONE);
                                    layoutTimer.setVisibility(View.GONE);
                                    layoutSelesai.setVisibility(View.VISIBLE);
                                    if (sALLY>sENEMY){
                                        if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                            trophy_text.setText("ANDA MENANG DUEL");
                                        }else{
                                            trophy_text.setText("ANDA KALAH DUEL");
                                        }
                                    }else if (sALLY==sENEMY){
                                        trophy_text.setText("DRAW");
                                    }else{
                                        if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                            trophy_text.setText("ANDA KALAH DUEL");
                                        }else{
                                            trophy_text.setText("ANDA MENANG DUEL");
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("ERROR DUEL RUN",databaseError.getMessage());
                            }
                        });

                    }else{
                        lblState.setText("Menunggu Hasil Lawan");

                        new CountDownTimer(4 * 15 * 1000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                int seconds = (int) (millisUntilFinished / 1000) % 60;
                                int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);

                                if (minutes > 0) {
                                    lblTimer.setText(minutes + " Menit " + seconds + " Detik");
                                } else {
                                    lblTimer.setText(seconds + " Detik");
                                }

                                if(stateALLY==6 && stateENEMY==6){
                                    cancel();
                                }
                            }

                            public void onFinish() {
                                mListener = firebaseDB.child("duel").child("run").child(ROOM).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                                        if (dataSnapshot.getChildrenCount()>0) {
                                            for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                                                DataSnapshot Room = arrRoom.iterator().next();
                                                if (Room.getKey().equalsIgnoreCase(ALLY)){
                                                    sALLY = AppConfig.NullNumber(Room.getValue());
                                                    scoreAlly.setText(Room.getValue().toString());
                                                }else if (Room.getKey().equalsIgnoreCase(ENEMY)){
                                                    sENEMY = AppConfig.NullNumber(Room.getValue());
                                                    scoreEnemy.setText(Room.getValue().toString());
                                                }
                                            }

                                            lblHasil.setVisibility(View.GONE);
                                            layoutTimer.setVisibility(View.GONE);
                                            layoutSelesai.setVisibility(View.VISIBLE);
                                            if (sALLY>sENEMY){
                                                if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                                    trophy_text.setText("ANDA MENANG DUEL");
                                                }else{
                                                    trophy_text.setText("ANDA KALAH DUEL");
                                                }
                                            }else if (sALLY==sENEMY){
                                                trophy_text.setText("DRAW");
                                            }else{
                                                if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                                    trophy_text.setText("ANDA KALAH DUEL");
                                                }else{
                                                    trophy_text.setText("ANDA MENANG DUEL");
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("ERROR DUEL RUN",databaseError.getMessage());
                                    }
                                });
                            }
                        }.start();
                    }

                }
            }
        };
        timer.start();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
        hasilListener = firebaseDB.child("duel").child("state").child(ROOM).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    stateALLY=1;
                    stateENEMY=1;
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Room = arrRoom.iterator().next();
                        if (Room.getKey().equalsIgnoreCase(ALLY)){
                            stateALLY = AppConfig.NullNumber(Room.getValue());
                        }else if (Room.getKey().equalsIgnoreCase(ENEMY)){
                            stateENEMY = AppConfig.NullNumber(Room.getValue());
                        }
                    }
                    if(stateALLY==6 && stateENEMY==6){
                        FINISHED = true;
                        lblSoal.setText("DUEL TELAH BERAKHIR");
                        lblKategori.setVisibility(View.INVISIBLE);
                        layoutJawab.setVisibility(View.GONE);
                        lblHasil.setVisibility(View.GONE);
                        layoutTimer.setVisibility(View.GONE);
                        layoutSelesai.setVisibility(View.VISIBLE);
                        if (sALLY>sENEMY){
                            if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                trophy_text.setText("ANDA MENANG DUEL");
                            }else{
                                trophy_text.setText("ANDA KALAH DUEL");
                            }
                        }else if (sALLY==sENEMY){
                            trophy_text.setText("DRAW");
                        }else{
                            if(auth.getAkun().akun_nama.equalsIgnoreCase(ALLY)){
                                trophy_text.setText("ANDA KALAH DUEL");
                            }else{
                                trophy_text.setText("ANDA MENANG DUEL");
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR DUEL RUN",databaseError.getMessage());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    private void setJawab(int num, int pos){
        for (int i=0;i<resJawab.length;i++){
            col[i]=0;
            etJawab[i].setText("");
        }
        for(int i=0; i<num; i++){
            col[i] = 1;
        }

        for (int i=0;i<resJawab.length;i++){
            if (col[i]==1){etJawab[i].setVisibility(View.VISIBLE); if(pos==(i+1)){etJawab[i].setVisibility(View.INVISIBLE);etJawab[i].setText(" ");}}else{etJawab[i].setVisibility(View.GONE);}
        }

        etJawab[0].requestFocus();
    }

    private void setFormJawab(){
        for (int i=0;i<resJawab.length;i++) {
            final int index=i;

            etJawab[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (count >= 1 || AppConfig.NullString(etJawab[index].getText().toString()).length() >=1) {
                        if (!s.toString().equals(s.toString().toUpperCase())) {
                            etJawab[index].setText(s.toString().toUpperCase());
                            etJawab[index].setSelection(etJawab[index].getText().length());
                        }
                        if (index<resJawab.length-2){
                            if (col[index] == 1) {
                                etJawab[index+1].requestFocus();
                            } else {
                                etJawab[index+2].requestFocus();
                            }
                        }else if (index<resJawab.length-2){
                            etJawab[index+1].requestFocus();
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            if (index>0) {
                etJawab[i].setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_DEL) {
                            if (AppConfig.NullString(etJawab[index].getText()).equalsIgnoreCase("")) {
                                if (col[index]==1){
                                    etJawab[index - 1].requestFocus();
                                    etJawab[index - 1].setSelection(etJawab[index - 1].getText().length());
                                }else{
                                    if (index>1){
                                        etJawab[index - 2].requestFocus();
                                        etJawab[index - 2].setSelection(etJawab[index - 2].getText().length());
                                    }
                                }
                            }
                        }
                        return false;
                    }
                });
            }
        }
    }
}
