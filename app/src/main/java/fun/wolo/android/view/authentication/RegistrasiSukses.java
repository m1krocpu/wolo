package fun.wolo.android.view.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import fun.wolo.android.AppConfig;
import fun.wolo.android.R;

public class RegistrasiSukses extends AppCompatActivity {
    LinearLayout btRegistrasi;
    TextView lblHalo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi_sukses);

        lblHalo = findViewById(R.id.lblHalo);
        lblHalo.setText("Halo, " + getIntent().getStringExtra("USERNAME"));

        btRegistrasi = findViewById(R.id.btRegistrasi);
        btRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrasiSukses.this, AppConfig.MainApp.getClass());
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
