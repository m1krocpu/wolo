package fun.wolo.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.database.FirebaseDatabase;

import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.WoloService;
import fun.wolo.android.model.m_auth;
import fun.wolo.android.view.akun.Akun;
import fun.wolo.android.view.duel.DuelCari;
import fun.wolo.android.view.harian.HarianHome;
import fun.wolo.android.view.news.NewsEvent;
import fun.wolo.android.view.obrolan.ObrolanHome;
import fun.wolo.android.view.statistik.Statistik;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Main extends AppCompatActivity {
    m_auth auth;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        Launcher.intent = new Intent(getBaseContext(), WoloService.class);
        getBaseContext().bindService(Launcher.intent, Launcher.m_serviceConnection, BIND_AUTO_CREATE);
        startService(Launcher.intent);

        setContentView(R.layout.activity_main);
        auth = new m_auth(this);
    }


    public void showKuis(View v){
        Intent intent = new Intent(this, HarianHome.class);
        startActivity(intent);
    }
    public void showDuel(View v){
        Intent intent = new Intent(this, DuelCari.class);
        startActivity(intent);
    }
    public void showObrolan(View v){
        //new PromptInfo(this, "Cooming Soon").show();
        Intent intent = new Intent(this, ObrolanHome.class);
        startActivity(intent);
    }
    public void showStatistik(View v){
        Intent intent = new Intent(this, Statistik.class);
        startActivity(intent);
    }
    public void showAkun(View v){
        Intent intent = new Intent(this, Akun.class);
        startActivityForResult(intent,0);
    }
    public void showEvent(View v){
        Intent intent = new Intent(this, NewsEvent.class);
        startActivity(intent);
    }
    public void showTanya(View v){
        Intent intent = new Intent(this, FAQS.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==0){
            if (resultCode==1){
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!auth.getLastRoom().equalsIgnoreCase("")) {
            FirebaseDatabase.getInstance().getReference().child("duel").child("room").child(auth.getLastRoom()).removeValue();
            auth.setLastRoom("");
        }
        Launcher.activityResumed();
    }

    @Override
    public void onBackPressed() {
        Launcher.activityPaused();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        Launcher.activityPaused();
        super.onPause();
    }


}
