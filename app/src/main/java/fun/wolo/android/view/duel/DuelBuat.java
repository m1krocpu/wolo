package fun.wolo.android.view.duel;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DuelBuat extends AppCompatActivity {
    String ROOM="", STATE="CREATE";
    TextView nickAlly, lokasiAlly, nickEnemy, lokasiEnemy, waiting, lblTimer, lblInfo;
    LinearLayout enemy, btKabur;
    CountDownTimer timer;
    String nAlly, lAlly, nEnemy, lEnemy;
    DatabaseReference firebaseDB;
    ValueEventListener mListener,waitingListener, soalListener;

    HashMap<String, Object> DUEL = new HashMap<>();
    m_auth auth;
    boolean started = false;
    Connector conn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_duel_buat);
        auth = new m_auth(this);
        conn = new Connector(this);
        Launcher.DUELSOAL.clear();

        firebaseDB = FirebaseDatabase.getInstance().getReference();

        ROOM = getIntent().getStringExtra("ROOM");
        STATE = getIntent().getStringExtra("STATE");

        DUEL.put("p1", auth.getAkun().akun_nama);
        DUEL.put("d1", auth.getAkun().akun_desa_nama);

        //firebaseDB.child("duel").child("room").child("ROOM").setValue(DUEL);

        nickAlly = findViewById(R.id.nickAlly);
        lokasiAlly = findViewById(R.id.lokasiAlly);
        nickEnemy = findViewById(R.id.nickEnemy);
        lokasiEnemy = findViewById(R.id.lokasiEnemy);
        waiting = findViewById(R.id.waiting);
        enemy = findViewById(R.id.enemy);
        lblInfo = findViewById(R.id.lblInfo);
        lblTimer = findViewById(R.id.lblTimer);
        btKabur = findViewById(R.id.btKabur);

        if (STATE.equalsIgnoreCase("CREATE")) {
            timer = new CountDownTimer(60 * 60 * 1000, 1000) {
                public void onTick(long millisUntilFinished) {
                    long millis = (60 * 60 * 1000) - millisUntilFinished;

                    int seconds = (int) (millis / 1000) % 60;
                    int minutes = (int) ((millis / (1000 * 60)) % 60);
                    int hours = (int) ((millis / (1000 * 60 * 60)) % 24);

                    if (hours > 0) {
                        lblTimer.setText(hours + " Jam " + minutes + " Menit " + seconds + " Detik");
                    } else if (minutes > 0) {
                        lblTimer.setText(minutes + " Menit " + seconds + " Detik");
                    } else {
                        lblTimer.setText(seconds + " Detik");
                    }
                }

                public void onFinish() {
                    lblInfo.setText("Persiapan Duel!");
                }
            };
            timer.start();

            waitingListener=  firebaseDB.child("duel").child("room").child(ROOM).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                    if (dataSnapshot.getChildrenCount() > 0) {
                        for (int i = 0; i < dataSnapshot.getChildrenCount(); i++) {
                            DataSnapshot Room = arrRoom.iterator().next();
                            if (Room.getKey().equalsIgnoreCase("p1")) {
                                nickAlly.setText("@" + Room.getValue().toString());
                            } else if (Room.getKey().equalsIgnoreCase("d1")) {
                                lokasiAlly.setText(Room.getValue().toString());
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("ERROR DUEL RUN", databaseError.getMessage());
                }
            });
        }else{
            btKabur.setVisibility(View.INVISIBLE);
            lblInfo.setText("Persiapan Duel!");
        }

        mListener= firebaseDB.child("duel").child("run").child(ROOM).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Room = arrRoom.iterator().next();
                        if (Room.getKey().equalsIgnoreCase("p1")){
                            nAlly = Room.getValue().toString();
                            nickAlly.setText("@"+Room.getValue().toString());
                        }else if (Room.getKey().equalsIgnoreCase("d1")){
                            lAlly = Room.getValue().toString();
                            lokasiAlly.setText(Room.getValue().toString());
                        }if (Room.getKey().equalsIgnoreCase("p2")){
                            nEnemy = Room.getValue().toString();
                            nickEnemy.setText("@"+Room.getValue().toString());
                            waiting.setVisibility(View.GONE);
                            enemy.setVisibility(View.VISIBLE);
                        }else if (Room.getKey().equalsIgnoreCase("d2")){
                            lEnemy = Room.getValue().toString();
                            lokasiEnemy.setText(Room.getValue().toString());
                        }
                    }
                    if (!lokasiEnemy.getText().toString().trim().equalsIgnoreCase("")){
                        if (STATE.equalsIgnoreCase("CREATE")) {
                            conn.soalDuel(ROOM);
                            timer.cancel();
                        }
                        if (!Launcher.isActivityVisible()){
                            showNotif();
                        }
                        btKabur.setVisibility(View.INVISIBLE);
                        lblInfo.setText("Persiapan Duel!");
                        soalListener = firebaseDB.child("duel").child("soal").child(ROOM).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Launcher.DUELSOAL.clear();
                                HashMap<String, Object> item;
                                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                                if (dataSnapshot.getChildrenCount()>0) {
                                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                                        DataSnapshot KATEGORI = arrRoom.iterator().next();
                                        if (KATEGORI.getChildrenCount()>0){
                                            Iterable<DataSnapshot> arrKategori = KATEGORI.getChildren();
                                            item = new HashMap<>();
                                            for (int j=0; j<KATEGORI.getChildrenCount();j++) {
                                                DataSnapshot SOAL = arrKategori.iterator().next();
                                                item.put(SOAL.getKey(), SOAL.getValue());

                                                Launcher.DUELSOAL.put(KATEGORI.getKey(), item);
                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("ERROR DUEL RUN",databaseError.getMessage());
                            }
                        });
                        startTimer();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR DUEL RUN",databaseError.getMessage());
            }
        });
    }
    public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mListener!=null){firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(mListener);}
        if (waitingListener!=null){firebaseDB.child("duel").child("room").child(ROOM).removeEventListener(waitingListener);}
        if (soalListener!=null){firebaseDB.child("duel").child("soal").child(ROOM).removeEventListener(soalListener);}
    }

    private void startTimer(){
        new CountDownTimer(5 * 60 * 1000, 1000) {
            long millis=0;
            public void onTick(long millisUntilFinished) {
                millis+=1000;
                int seconds = (int) (millis / 1000) % 60 ;
//                if (millis==(2 * 60 * 1000)){
//                    conn.cancel();
//                    conn.soalDuel(ROOM);
//                }

                lblTimer.setText(String.valueOf(seconds));
                if (!started && Launcher.DUELSOAL.size()>=5){
                    started = true;
                    cancel();
                    if (mListener!=null){firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(mListener);}
                    if (waitingListener!=null){firebaseDB.child("duel").child("room").child(ROOM).removeEventListener(waitingListener);}

                    Intent intent = new Intent(DuelBuat.this, DuelRun.class);
                    intent.putExtra("ROOM",ROOM);
                    intent.putExtra("nALLY",nAlly);
                    intent.putExtra("lALLY",lAlly);
                    intent.putExtra("nENEMY",nEnemy);
                    intent.putExtra("lENEMY",lEnemy);
                    startActivity(intent);
                    finish();
                }
            }

            public void onFinish() {
                if (!started && Launcher.DUELSOAL.size()>=5) {
                    started = true;
                    if (mListener != null) {
                        firebaseDB.child("duel").child("run").child(ROOM).removeEventListener(mListener);
                    }
                    if (waitingListener != null) {
                        firebaseDB.child("duel").child("room").child(ROOM).removeEventListener(waitingListener);
                    }

                    Intent intent = new Intent(DuelBuat.this, DuelRun.class);
                    intent.putExtra("ROOM", ROOM);
                    intent.putExtra("nALLY", nAlly);
                    intent.putExtra("lALLY", lAlly);
                    intent.putExtra("nENEMY", nEnemy);
                    intent.putExtra("lENEMY", lEnemy);
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    private void showNotif(){
        Intent notificationIntent = new Intent(getApplicationContext(), DuelBuat.class);
        notificationIntent.putExtra("ROOM",ROOM);
        notificationIntent.putExtra("STATE",STATE);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(DuelBuat.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
        Notification notification = builder.setContentTitle("Duel Kuis telah menemukan Lawan")
                .setContentText("Klik Notifikasi Ini untuk melanjutkan")
                .setTicker("Duel Kuis telah menemukan Lawan")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVibrate(new long[]{300, 300, 300, 300, 300})
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}
