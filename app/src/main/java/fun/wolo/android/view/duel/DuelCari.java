package fun.wolo.android.view.duel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.adapter.DuelAdapter;
import fun.wolo.android.lib.Connector;
import fun.wolo.android.lib.PromptInfo;
import fun.wolo.android.lib.PromptLoading;
import fun.wolo.android.model.m_auth;
import fun.wolo.android.model.m_duel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DuelCari extends AppCompatActivity {
    DuelAdapter adapter;
    ArrayList<m_duel> data = new ArrayList<>();
    RecyclerView lvData;
    SwipeRefreshLayout layout;

    Connector conn;
    m_auth auth;
    PromptLoading pDialog;
    FrameLayout overlay;
    LinearLayout empty;
    DatabaseReference firebaseDB;
    ValueEventListener dataListener;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_duel_cari);
        auth = new m_auth(this);
        conn = new Connector(this);
        firebaseDB = FirebaseDatabase.getInstance().getReference();

        overlay = findViewById(R.id.overlay);
        empty = findViewById(R.id.empty);
        layout = findViewById(R.id.layout);
        layout.setColorSchemeResources(R.color.mainPrimary, R.color.mainPrimary, android.R.color.white);
        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setData();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        lvData = findViewById(R.id.lvData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new DuelAdapter(this, data, new DuelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final m_duel item, int position) {
                overlay.setVisibility(View.VISIBLE);

                pDialog = new PromptLoading(DuelCari.this);
                pDialog.setMessage("Bergabung Duel Kuis " + String.valueOf(item.getNomor()) + " ...");
                pDialog.setCancelable(false);
                pDialog.setOnFalseListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pDialog.dismiss();
                        conn.cancel();
                    }
                });
                pDialog.show();
                conn.joinDuel(String.valueOf(item.getNomor()), new Connector.OnCompletedListener() {
                    @Override
                    public void OnCompleted(Object data) {
                        pDialog.dismiss();
                        overlay.setVisibility(View.GONE);

                        Intent intent = new Intent(DuelCari.this, DuelBuat.class);
                        intent.putExtra("STATE","JOIN");
                        intent.putExtra("ROOM", String.valueOf(item.getNomor()));
                        startActivity(intent);
                    }

                    @Override
                    public void OnFailed(int status) {
                        pDialog.dismiss();
                        overlay.setVisibility(View.GONE);

                        if (status==1){
                            new PromptInfo(DuelCari.this,"Gagal Bergabung Duel Kuis "+ String.valueOf(item.getNomor()) +", silahkan coba lagi atau pilih Duel Kuis lain!").show();
                        }
                    }
                });
            }
        });
        lvData.setHasFixedSize(true);
        lvData.setLayoutManager(layoutManager);
        lvData.setAdapter(adapter);

        setData();
    }

    public void back(View v){
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!auth.getLastRoom().equalsIgnoreCase("")) {
            FirebaseDatabase.getInstance().getReference().child("duel").child("room").child(auth.getLastRoom()).removeValue();
            auth.setLastRoom("");
        }
        Launcher.activityResumed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    public void showDuelBuat(View v){
        overlay.setVisibility(View.VISIBLE);

        pDialog = new PromptLoading(this);
        pDialog.setMessage("Membuat Duel Kuis...");
        pDialog.setCancelable(false);
        pDialog.setOnFalseListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog.dismiss();
                conn.cancel();
            }
        });
        pDialog.show();
        conn.setDuel(new Connector.OnCompletedListener() {
            @Override
            public void OnCompleted(Object data) {
                auth.setLastRoom(data.toString());

                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                Intent intent = new Intent(DuelCari.this, DuelBuat.class);
                intent.putExtra("STATE","CREATE");
                intent.putExtra("ROOM",data.toString());
                startActivity(intent);
            }

            @Override
            public void OnFailed(int status) {
                pDialog.dismiss();
                overlay.setVisibility(View.GONE);
                if (status==1){
                    new PromptInfo(DuelCari.this,"Duel Kuis gagal dibuat, silahkan coba lagi!").show();
                }
            }
        });
    }

    private void setData(){
        if (dataListener!=null){
            firebaseDB.child("duel").child("room").removeEventListener(dataListener);
        }

        dataListener = firebaseDB.child("duel").child("room").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                data.clear();
                Iterable<DataSnapshot> arrRoom = dataSnapshot.getChildren();
                if (dataSnapshot.getChildrenCount()>0) {
                    for (int i=0; i<dataSnapshot.getChildrenCount();i++){
                        DataSnapshot Room = arrRoom.iterator().next();
                        String KEY = Room.getKey();
                        m_duel dataRoom = new m_duel();
                        dataRoom.setNomor(Long.parseLong(KEY));
                        if (Room.getChildrenCount()>0) {
                            Iterable<DataSnapshot> column = Room.getChildren();
                            for (int j=0; j<Room.getChildrenCount();j++){
                                DataSnapshot data = column.iterator().next();
                                if (data.getKey().equalsIgnoreCase("p1")){
                                    dataRoom.setNick(data.getValue().toString());
                                }else if (data.getKey().equalsIgnoreCase("d1")){
                                    dataRoom.setLokasi(data.getValue().toString());
                                }
                            }
                            if (dataRoom.getNick()!=null && dataRoom.getLokasi()!=null){
                                if (!dataRoom.getNick().equalsIgnoreCase(auth.getAkun().akun_nama) && !dataRoom.getLokasi().equalsIgnoreCase(auth.getAkun().akun_desa_nama)){
                                    data.add(dataRoom);
                                    adapter.notifyDataSetChanged();
                                    if (data.size()<=0){
                                        empty.setVisibility(View.VISIBLE);
                                    }else{
                                        empty.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }
                    }
                }else{
                    adapter.notifyDataSetChanged();
                    empty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR ROOM DUEL",databaseError.getMessage());
            }
        });
    }
}
