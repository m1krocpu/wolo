package fun.wolo.android.view.obrolan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import fun.wolo.android.AppConfig;
import fun.wolo.android.Launcher;
import fun.wolo.android.R;
import fun.wolo.android.model.m_auth;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ObrolanHome extends AppCompatActivity {
    ValueEventListener publikListener, desaListener, kecamatanListener, kotaListener;
    DatabaseReference firebaseDB;
    TextView lblLastPublik, lblLastDesa, lblLastKecamatan, lblLastKota, lblLokasiDesa, lblLokasiKecamatan, lblLokasiKota, lblPublik, lblDesa, lblKecamatan, lblKota;
    m_auth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_obrolan_home);
        auth = new m_auth(this);
        firebaseDB = FirebaseDatabase.getInstance().getReference();

        lblLastPublik = findViewById(R.id.lblLastPublik);
        lblLastDesa = findViewById(R.id.lblLastDesa);
        lblLastKecamatan = findViewById(R.id.lblLastKecamatan);
        lblLastKota = findViewById(R.id.lblLastKota);
        lblLokasiDesa = findViewById(R.id.lblLokasiDesa);
        lblLokasiKecamatan = findViewById(R.id.lblLokasiKecamatan);
        lblLokasiKota = findViewById(R.id.lblLokasiKota);
        lblPublik = findViewById(R.id.lblPublik);
        lblDesa = findViewById(R.id.lblDesa);
        lblKecamatan = findViewById(R.id.lblKecamatan);
        lblKota = findViewById(R.id.lblKota);

        lblLokasiDesa.setText(auth.getAkun().akun_desa_nama);
        lblLokasiKecamatan.setText(auth.getAkun().akun_kecamatan_nama);
        lblLokasiKota.setText(auth.getAkun().akun_kota_nama);

        publikListener = firebaseDB.child("obrolan").child("last").child("publik").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblLastPublik.setText(AppConfig.NullString(dataSnapshot.getValue()));
            }@Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR CHAT",databaseError.getMessage());
            }
        });
        desaListener = firebaseDB.child("obrolan").child("last").child("desa_"+auth.getAkun().akun_desa_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblLastDesa.setText(AppConfig.NullString(dataSnapshot.getValue()));
            }@Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR CHAT",databaseError.getMessage());
            }
        });
        kecamatanListener = firebaseDB.child("obrolan").child("last").child("kecamatan_"+auth.getAkun().akun_kecamatan_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblLastKecamatan.setText(AppConfig.NullString(dataSnapshot.getValue()));
            }@Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR CHAT",databaseError.getMessage());
            }
        });
        kotaListener = firebaseDB.child("obrolan").child("last").child("kota_"+auth.getAkun().akun_kota_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblLastKota.setText(AppConfig.NullString(dataSnapshot.getValue()));
            }@Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR CHAT",databaseError.getMessage());
            }
        });

        firebaseDB.child("obrolan").child("audiensi").child("publik").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblPublik.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR AUDIENSI",databaseError.getMessage());
            }
        });

        firebaseDB.child("obrolan").child("audiensi").child("desa_"+auth.getAkun().akun_desa_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblDesa.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR AUDIENSI",databaseError.getMessage());
            }
        });

        firebaseDB.child("obrolan").child("audiensi").child("kecamatan_"+auth.getAkun().akun_kecamatan_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblKecamatan.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR AUDIENSI",databaseError.getMessage());
            }
        });

        firebaseDB.child("obrolan").child("audiensi").child("kota_"+auth.getAkun().akun_kota_nama).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lblKota.setText(String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ERROR AUDIENSI",databaseError.getMessage());
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Launcher.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Launcher.activityPaused();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void back(View v){
        firebaseDB.child("obrolan").child("last").child("publik").removeEventListener(publikListener);
        firebaseDB.child("obrolan").child("last").child(auth.getAkun().akun_desa_nama).removeEventListener(desaListener);
        firebaseDB.child("obrolan").child("last").child(auth.getAkun().akun_kecamatan_nama).removeEventListener(kecamatanListener);
        firebaseDB.child("obrolan").child("last").child(auth.getAkun().akun_kota_nama).removeEventListener(kotaListener);
        finish();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        firebaseDB.child("obrolan").child("last").child("publik").removeEventListener(publikListener);
        firebaseDB.child("obrolan").child("last").child("desa_"+auth.getAkun().akun_desa_nama).removeEventListener(desaListener);
        firebaseDB.child("obrolan").child("last").child("kecamatan_"+auth.getAkun().akun_kecamatan_nama).removeEventListener(kecamatanListener);
        firebaseDB.child("obrolan").child("last").child("kota_"+auth.getAkun().akun_kota_nama).removeEventListener(kotaListener);
        super.onBackPressed();
        this.overridePendingTransition(R.anim.nothing, android.R.anim.fade_out);
    }

    public void showPublik(View v){
        Intent intent = new Intent(this, ObrolanChat.class);
        intent.putExtra("STATE","publik");
        intent.putExtra("TITLE","publik");
        startActivity(intent);
    }

    public void showDesa(View v){
        Intent intent = new Intent(this, ObrolanChat.class);
        intent.putExtra("STATE","desa_"+auth.getAkun().akun_desa_nama);
        intent.putExtra("TITLE",auth.getAkun().akun_desa_nama);
        startActivity(intent);
    }

    public void showKecamatan(View v){
        Intent intent = new Intent(this, ObrolanChat.class);
        intent.putExtra("STATE","kecamatan_"+auth.getAkun().akun_kecamatan_nama);
        intent.putExtra("TITLE",auth.getAkun().akun_kecamatan_nama);
        startActivity(intent);
    }

    public void showKota(View v){
        Intent intent = new Intent(this, ObrolanChat.class);
        intent.putExtra("STATE","kota_"+auth.getAkun().akun_kota_nama);
        intent.putExtra("TITLE",auth.getAkun().akun_kota_nama);
        startActivity(intent);
    }
}
